# **Useful Python Code**  
(tested with Python 3.8)

---
**1. List files of particular types in a directory**  
[files\_in\_dir.py](./code/files_in_dir/files_in_dir.py)

**2. Natural sorting then renaming of files in a directory**  
[sort\_rename\_numerical.py](./code/files_rename_numerical/sort_rename_numerical.py)

**3. Rename files numerically, then copy to new directory**  
[renumber\_single\_dir.py](./code/files_copy_renamed_new_dir/renumber_single_dir.py)   

**4. Write and read CSV configuration files**  
[csv\_config.py](./code/csv_config/csv_config.py)

**5. Count unique words in an input file**  
[cuw.py](./code/count_unique_words/cuw.py)  
[input.txt](./code/count_unique_words/input.txt)  

**6. Launch a program, then kill process from keyboard**  
[launch\_then\_terminate\_kb.py](./code/launch_then_terminate_kb/launch_then_terminate_kb.py)

**7. Calculate, then apply a non-uniformity correction to images**  
[calculate\_sensor\_nuc.py](./code/sensor_NUC/calculate_sensor_nuc.py)  
[read\_apply\_nuc.py](./code/sensor_NUC/read_apply_nuc.py)  

**8. Tkinter - a user interface example**  
[kana.py](./code/tkinter_example/kana.py)  

**9. SQL - using SQLite3 from the command line, using sqlite3 from Python, and using pandasql**  
[create\_tables\_from\_csv.py](./code/SQL/create_tables_from_csv.py)  
[cli_examples](./code/SQL/cli_examples.md)  
[EDA\_reddit\_posts.ipynb](./code/SQL/EDA_reddit_posts.ipynb)  

**10. Monitor a directory for changes - the _Watcher_ class**  
[dir\_watcher](./code/dir_watcher) - a simple dir watcher  
[dir\_watcher_2](./code/dir_watcher_2) - with logging and settings file  

**11. Search for the first occurrence of a key in a nested dictionary and return its value**  
[main calling program](./code/dict_traversal/main.py)  
[function definition](./code/dict_traversal/dict_traverse.py)  

**12. Data Structures: Graphs**  
[Graph Class with search methods](./code/data_structures/graphs/readme.md) - applied to a 2d connectivity problem  

---

## Useful Python Knowledge

**1. Answers to whiteboarding problems and interview questions**  
[my notes](./code/whiteboarding/readme.md)  





