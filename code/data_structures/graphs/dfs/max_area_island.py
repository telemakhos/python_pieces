from collections import defaultdict

class Graph:

    def __init__(self):
        self.graph = defaultdict(list)
        self.visited = set()
        self.count = 0
        self.max_area = 0

    def add_vertex(self, v):
        self.graph[v] = []
 
    def add_edge(self, u, v):
        self.graph[u].append(v)

    def dfs_recur(self, v):
        self.visited.add(v)
        #print(v,end=" ")
        self.count += 1
 
        for neighbor in self.graph[v]:
            if neighbor not in self.visited:
                self.dfs_recur(neighbor)

    def dfs(self):
        for vertex in self.graph:
            self.count = 0
            if vertex not in self.visited:
                self.dfs_recur(vertex)
                self.max_area = max(self.max_area, self.count) 
        #print("")


############################################### 

# define the island world as an array (2d list)
grid = [
    [0,0,1,0,0,0,0,1,0,0,0,0,0],
    [0,0,0,0,0,0,0,1,1,1,0,0,0],
    [0,1,1,0,1,0,0,0,0,0,0,0,0],
    [0,1,0,0,1,1,0,0,1,0,1,0,0],
    [0,1,0,0,1,1,0,0,1,1,1,0,0],
    [0,0,0,0,0,0,0,0,0,0,1,0,0],
    [0,0,0,0,0,0,0,1,1,1,0,0,0],
    [0,0,0,0,0,0,0,1,1,0,0,0,0]
]

nrows = len(grid)
ncols = len(grid[0])

# instantiate a graph object
g = Graph()

# identify neighboring 1's
def get_neighbors(r,c,nrows, ncols):
    ns = []
    if r-1 > 0:
        if grid[r-1][c] == 1: ns.append((r-1,c))
    if r+1 < nrows:
        if grid[r+1][c] == 1: ns.append((r+1,c))
    if c-1 > 0:
        if grid[r][c-1] == 1: ns.append((r,c-1))
    if c+1 < ncols:
        if grid[r][c+1] == 1: ns.append((r,c+1))
    return ns

# add any neighbors as edges to this graph
for r in range(nrows):
    for c in range(ncols):
        if grid[r][c] == 1:
            g.add_vertex((r,c))
            neighbors = get_neighbors(r,c,nrows,ncols)
            for n in neighbors:
                g.add_edge((r,c), n)

g.dfs()
print("max area:", g.max_area)


<<<<<<< HEAD
=======

>>>>>>> 13d883ff6c375ca1c190165641e7bb2993b7cb2e
