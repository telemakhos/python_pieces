'''
    Get images from the camera
'''
import os
import time
from pathlib import Path  # TESTING ONLY
import socket
from threading import Thread
import queue
import numpy as np



HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 13000        # The port used by the server
n_bytes = 2048      # size, in bytes, of the socket recieve buffer


def ack(msg):
    if msg.decode("UTF-8").split(" ")[-1].split("\n")[0] == "OK":
        return True
    else:
        return False
 

#################################################################################
class Camera(Thread):
    def __init__(self, to_camera_queue, from_camera_queue, auto_grab_path):
        # Call Thread constructor
        super().__init__()
        self.keep_running = True
        self.to_camera_queue = to_camera_queue # for receiving messages from main program
        self.from_camera_queue = from_camera_queue # for sending messsages to main program
        self.auto_grab_path = auto_grab_path
        self.imgH = 128
        self.imgW = 128
        self.IROW_SIGNAL = 64
        self.nb_frames_to_grab = 128
        self.lock = False # prevents double saving with the NFRAMES button
        
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((HOST, PORT))
        self.s.sendall(b'SET NUM_FRAMES 128\n')
        data = self.s.recv(n_bytes)
        if not ack(data):
            print("couldn't set camera NUM_FRAMES.")
        self.s.sendall(b'SET PREFIX AUTO\n')
        data = self.s.recv(n_bytes)        
        if not ack(data):
            print("couldn't set camera PREFIX.")
        
        
    def set_dset(self, dset):
        # this is NULL until get_N_frames is called
        self.dset = dset

    def set_data_dir(self, ddir):
        self.data_dir = str(ddir)

    def stop(self):
        # Call this from another thread to stop the receiver
        self.s.sendall(b'EXIT\n') # signal camera communication software to exit
        data = self.s.recv(n_bytes)
        if not ack(data):
            print("couldn't exit camera software properly.")
        self.keep_running = False # terminate this thread

    def run(self):
        # This will run when you call .start method
        while self.keep_running:
            if self.to_camera_queue.qsize():
                msg = self.to_camera_queue.get()

                if (msg == "SINGLE_FRAME"): # grab a single frame for the live-camera display
                    #print(" single frame ...")
                    self.get_single_frame()

                elif (msg == "N_FRAMES"): # manual grab of N frames
                    #print(" N_frames ...")
                    self.get_N_frames(self.nb_frames_to_grab)

                elif (msg == "N_FRAMES_MEASUREMENT"): # spectral measurements (stage positions)
                    #print(" N_frames spectral measurement...")
                    self.get_N_frames_meas(self.nb_frames_to_grab)

            time.sleep(0.05)

    def get_single_frame(self):
        # send TCP commands
        cmdstr = 'SET PATH ' + str(self.auto_grab_path) + '\n'
        self.s.sendall(cmdstr.encode(encoding='UTF-8'))
        #self.s.sendall(b'SET PATH c:\\AEDC\\RawDataFiles\n')
        data = self.s.recv(n_bytes)
        if not ack(data):
            print("couldn't set path.")
            return 0

        self.s.sendall(b'SET PREFIX AUTO\n')
        data = self.s.recv(n_bytes)
        
        self.s.sendall(b'GET SINGLE_FRAME SINGLE_FRAME.RAW\n')
        data = self.s.recv(n_bytes)
        if not ack(data):
            print("couldn't GET SINGLE_FRAME")
            return 0
        self.lock = False
        self.from_camera_queue.put("OK SINGLE")

    def get_N_frames(self, N):
        if self.lock == False:            
            # send TCP commands
            cmdstr = 'SET PATH ' + str(self.auto_grab_path) + '\n'
            self.s.sendall(cmdstr.encode(encoding='UTF-8'))
            #self.s.sendall(b'SET PATH c:\\AEDC\\RawDataFiles\n')
            data = self.s.recv(n_bytes)
            if not ack(data):
                print("couldn't set NFRAMES path")
                return 0

            self.s.sendall(b'SET PREFIX MANUAL\n')
            data = self.s.recv(n_bytes) 
            
            self.s.sendall(b'GET DATA_FILE NFRAMES.RAW\n')
            data = self.s.recv(n_bytes)
            if not ack(data):
                print("couldn't Get Data File Nframes")
                return 0

            self.lock = True
            self.to_camera_queue.put("OK NFRAMES")        

    def get_N_frames_meas(self, N):
        if self.lock == False:

            # send TCP commands
            cmdstr = 'SET PATH ' + str(self.data_dir) + '\n'
            self.s.sendall(cmdstr.encode(encoding='UTF-8'))
            data = self.s.recv(n_bytes)
            if not ack(data):
                print("unable to SET PATH")
                return 0 

            cmdstr = 'SET PREFIX ' + str(self.dset) + '\n'
            self.s.sendall(cmdstr.encode(encoding='UTF-8'))
            data = self.s.recv(n_bytes)
            if not ack(data):
                print("unable to SET PREFIX")
                return 0 

            self.s.sendall(b'GET DATA_FILE NFRAMES.RAW\n')
            data = self.s.recv(n_bytes)
            if not ack(data):
                print("unable to GET DATA_FILE NFRAMES")
                return 0

            self.to_camera_queue.put("OK NFRAMES MEASUREMENT")        
 

#################################################################################
class CameraVirtual(Thread):
    def __init__(self, to_camera_queue, from_camera_queue, auto_grab_path):
        # Call Thread constructor
        super().__init__()
        self.keep_running = True
        self.to_camera_queue = to_camera_queue
        self.from_camera_queue = from_camera_queue
        self.auto_grab_path = auto_grab_path
        self.imgH = 128
        self.imgW = 128
        self.IROW_SIGNAL = 64
        self.nb_frames_to_grab = 128 # default
        self.get_single_frame()
        self.lock = False # prevents double saving with the NFRAMES button
        
    def set_dset(self, dset):
        # this is NULL until get_N_frames is called
        self.dset = dset

    def set_data_dir(self, ddir):
        self.data_dir = str(ddir)

    def stop(self):
        # Call this from another thread to stop the receiver
        self.keep_running = False

    def run(self):
        # This will run when you call .start method
        while self.keep_running:

            if self.to_camera_queue.qsize():
                msg = self.to_camera_queue.get()

                if (msg == "SINGLE_FRAME"): # grab a single frame for the live-camera display
                    #print(" single frame ...")
                    self.get_single_frame()

                elif (msg == "N_FRAMES"): # manual grab of N frames
                    #print(" N_frames ...")
                    self.get_N_frames()

                elif (msg == "N_FRAMES_MEASUREMENT"): # spectral measurements (stage positions)
                    #print(" N_frames spectral measurement...")
                    self.get_N_frames_meas()

            time.sleep(0.05)

    def get_single_frame(self):

        imgH, imgW = (128,128)
        bkg_level = 21000
        Image_bkg = np.random.randint(bkg_level//1.5, bkg_level, size=(imgH,imgW))
        Image = np.random.randint(50000, 65535, size=(imgH,imgW))
        mask = np.zeros((imgH,imgW))
        mask[61,:] = 0.1
        mask[62,:] = 0.4
        mask[63,:] = 1.0
        mask[64,:] = 0.4
        mask[65,:] = 0.1
    
        # apply a pdf so signal is highest in center
        mu = 0
        sigma = 35
        pdf = [ 1/(sigma * np.sqrt(2 * np.pi)) * np.exp( - (x - mu)**2 / (2 * sigma**2))
                for x in range(-(imgW//2),imgW//2) ]
        pdf = [70*x for x in pdf]
        pdf = np.array(pdf)
        mask[61,:] += pdf
        mask[62,:] += pdf
        mask[63,:] += pdf
        mask[64,:] += pdf
        mask[65,:] += pdf
    
        flimg = Image.astype(np.float64)
        flimg *= mask
        Image = flimg
        Image[0:61,:] = Image_bkg[0:61,:]
        Image[66:,:] = Image_bkg[66:,:]
        maxVal = np.max(Image)
        Image = Image.astype(np.float64)/maxVal
        Image *= 16383
        Image = Image.astype(np.uint16)

        self.image = Image

        #self.image = np.random.randint(16383, size=(128,128), dtype=np.uint16)
        filepath = os.path.join(self.auto_grab_path, "SINGLE_FRAME.RAW")

        self.image.astype('int16').tofile(filepath)
        self.lock = False
        self.from_camera_queue.put("OK SINGLE")
            
    def get_N_frames(self):
        if self.lock == False:
            N = self.nb_frames_to_grab
            self.image = np.random.randint(16383, size=(N, 128,128), dtype=np.uint16)
            outfile = os.path.join(self.data_dir, 'MANUAL_NFRAMES.RAW')
            self.image.astype('int16').tofile(str(outfile))
            self.lock = True
            self.from_camera_queue.put("OK NFRAMES")        

    def get_N_frames_meas(self):
        if self.lock == False:
            N = self.nb_frames_to_grab
            self.image = np.random.randint(16383, size=(N, 128,128), dtype=np.uint16)
            outfile = os.path.join(self.data_dir, self.dset+'_NFRAMES.RAW')
            self.image.astype('int16').tofile(str(outfile))
            self.from_camera_queue.put("OK NFRAMES MEASUREMENT")        
