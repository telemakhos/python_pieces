import os, time
import csv
from socket import *
from pathlib import Path
import threading
import queue
import random
import numpy as np
import tkinter as tk
from tkinter import ttk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import warnings
warnings.filterwarnings("ignore")

import temperature_controller


FIG_SIZE = (5.5,4.5)
BKG_COLOR = 'black'
FACE_COLOR = '#1a1a1a'
THEME_COLOR = 'limegreen'
LINE_COLOR1 = '#00b3b3'
LINE_COLOR2 = '#cc00cc'
FONTNAME = ''
XMIN, XMAX = (0,100) # adjustable plot min & max range values
LOGINTERVAL = 10

##################################################################################################
def get_current_time():
    return time.strftime('%y/%m/%d %H:%M:%S')

def get_YMD():
    return time.strftime('%Y_%m_%d')

def get_HMS():
    return time.strftime('%H:%M:%S')

##################################################################################################
class MplMap():

    @classmethod
    def settings(cls, root):
        cls.x = [0]
        cls.y1 = [0]
        cls.y2 = [0]
        plt.style.use('dark_background')
        cls.fig, cls.ax = plt.subplots(figsize=FIG_SIZE, dpi=100)
        cls.fig.patch.set_facecolor(FACE_COLOR)
        cls.ax.spines['left'].set_color(THEME_COLOR)
        cls.ax.spines['right'].set_color(THEME_COLOR)
        cls.ax.spines['top'].set_color(THEME_COLOR)
        cls.ax.spines['bottom'].set_color(THEME_COLOR)
        cls.ax.tick_params(axis='x', colors=THEME_COLOR)
        cls.ax.tick_params(axis='y', colors=THEME_COLOR)
        cls.ax.yaxis.label.set_color(THEME_COLOR)
        cls.ax.xaxis.label.set_color(THEME_COLOR)
        cls.ax.title.set_color(THEME_COLOR)
        cls.fig.tight_layout()
        cls.canvas = FigureCanvasTkAgg(cls.fig, master=root)
        
    @classmethod
    def get_canvas(cls):
        return cls.canvas

##################################################################################################
class PlotLine(MplMap):

    def __init__(self):
        self.channels = read_channels_file_csv()
        self.labels = read_channel_labels_file_csv() 
        self.root_dir = Path(__file__).parent.absolute()
        self.root_data_dir = os.path.join(self.root_dir, 'Data') 
        self.lw = 2
        self.xmin = XMIN
        self.xmax = XMAX
        self.nsecs = 1 # plot update rate in seconds
        self.plot_title = 'Stage Assembly Temperatures'
        self.master = None
        self.auto_update_plot = True
        self.x = []
        self.ys = [[] for _ in range(16)]
        self.markers = ['x','+','o','.']

    def clear_plot(self):
        self.ax.cla()

    def draw_plot(self):
        self.clear_plot()
        if self.x:
            if (self.x[-1] > self.xmax) and self.auto_update_plot:
                self.xmax += LOGINTERVAL * 5
        for ii in range(16):
            if self.channels[ii]:
                if len(self.ys[ii]) == len(self.x):
                    self.ax.plot(self.x, self.ys[ii], self.markers[ii%4],
                                 lw=self.lw, mfc='none', label=self.labels[ii])
        self.ax.set_xlabel('elapsed time (sec)')
        self.ax.set_ylabel('temperature (C)')
        self.ax.set_title(self.plot_title)
        self.ax.title.set_color(THEME_COLOR)
        self.ax.set_xlim(self.xmin,self.xmax)
        self.ax.legend(loc='upper right')
        self.fig.tight_layout()
        self.canvas.draw()

    def save_plot(self):
        subdirpath = Path(os.path.join(self.root_data_dir, 'temperature_monitor_logs'))
        subdirpath = Path(os.path.join(subdirpath, 'plot_saves'))
        subdirpath.mkdir(parents=True, exist_ok=True)
        ymd = ''.join(get_YMD().split('_'))
        hms = ''.join(get_HMS().split(':'))
        filename = ymd + '_' + hms + ".png"
        self.fig.savefig(os.path.join(subdirpath,filename))

    def set_xmin(self, x):
        if x < self.xmax:
            self.xmin = x

    def set_xmax(self, x):
        self.auto_update_plot = False
        if x > self.xmin:
            self.xmax = x

    def set_full_xscale(self):
        self.auto_update_plot = True
        self.xmin = 0
        if len(self.x) > 0:
            self.xmax = self.x[-1] + LOGINTERVAL
        else:
            self.xmax = 10

##################################################################################################
''' temperatureLogfile '''
class temperatureLogfile():
    def __init__(self):
        self.ymd = get_YMD()
        self.subdirpath = Path(os.path.join('Data', 'temperature_monitor_logs'))
        self.subdirpath.mkdir(parents=True, exist_ok=True)
        self.subdirpath = Path(os.path.join(self.subdirpath, 'temperatures'))
        self.subdirpath.mkdir(parents=True, exist_ok=True)
        self.filename = os.path.join(self.subdirpath, self.ymd + '.txt')
        if not os.path.isfile(self.filename):
            self.lf = open(self.filename, "w")
            csvwriter = csv.writer(self.lf, delimiter=',')
            csvwriter.writerow(["clock time","elapsed time","t_1","t_2","t_3","t_4"])
            self.close_logfile()
    
    def append(self, logtemps):
        ymd = get_YMD()
        if (ymd != self.ymd):
            self.ymd = ymd
            self.hms = hms
            self.filename = os.path.join(self.subdirpath, self.ymd + '.txt')
            self.lf = open(self.filename, "w")
            csvwriter = csv.writer(self.lf, delimiter=',')
            csvwriter.writerow(["clock time","elapsed time","t_1","t_2","t_3","t_4"])
            self.close_logfile()
        hms = get_HMS()
        self.open_logfile()
        csvwriter = csv.writer(self.lf, delimiter=',')
        csvwriter.writerow([hms] + logtemps)
        self.close_logfile()

    def open_logfile(self):
        self.lf = open(self.filename, "a")

    def close_logfile(self):
        self.lf.close()

##################################################################################################
''' errorLogfile '''
class errorLogfile():
    def __init__(self):
        self.ymd = get_YMD()
        self.subdirpath = Path(os.path.join('Data', 'temperature_monitor_logs'))
        self.subdirpath.mkdir(parents=True, exist_ok=True)
        self.subdirpath = Path(os.path.join(self.subdirpath, 'errors'))
        self.subdirpath.mkdir(parents=True, exist_ok=True)
        self.filename = os.path.join(self.subdirpath, self.ymd + '.txt')
        if not os.path.isfile(self.filename):
            self.lf = open(self.filename, "w")
            csvwriter = csv.writer(self.lf, delimiter=',')
            csvwriter.writerow(["clock time","elapsed time","ch_1","ch_2","ch_3","ch_4"])
            self.close_logfile()
    
    def append(self, errorlist):
        ymd = get_YMD()
        if (ymd != self.ymd):
            self.ymd = ymd
            self.hms = hms
            self.filename = os.path.join(self.subdirpath, self.ymd + '.txt')
            self.lf = open(self.filename, "w")
            csvwriter = csv.writer(self.lf, delimiter=',')
            csvwriter.writerow(["clock time","elapsed time","ch_1","ch_2","ch_3","ch_4"])
            self.close_logfile()
        hms = get_HMS()
        self.open_logfile()
        csvwriter = csv.writer(self.lf, delimiter=',')
        csvwriter.writerow([hms] + errorlist)
        self.close_logfile()

    def open_logfile(self):
        self.lf = open(self.filename, "a")

    def close_logfile(self):
        self.lf.close()

##################################################################################################
def read_channels_file_csv():
    subdirpath = Path("Config")
    onlyfiles = [f for f in os.listdir(subdirpath)
                 if os.path.isfile(os.path.join(subdirpath, f))] 
    channels_file = sorted([x for x in onlyfiles
                             if x.split(".csv")[0].endswith("temperature_monitor_channels")])
    channels = []
    # read the exposures file (if exists)
    if channels_file:
        fn = os.path.join(subdirpath, channels_file[0])
        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for i, line in enumerate(reader):
                if i>0 and line:
                    channels.append(int(line[0]))
    else:
        channels = [1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0]
        header = 'temperature monitors channels selected\n'
        filename = os.path.join(subdirpath, 'temperature_monitor_channels.csv')
        with open(filename, "a") as ap:
            if (os.path.getsize(filename)) <= 0:
                ap.write(header)
                for i in range(len(channels)):
                    thisVal = str(channels[i])
                    ap.write( thisVal +'\n' )
                ap.close()
    return channels

def write_channels(channels):
    subdirpath = Path("Config")
    filename = os.path.join(subdirpath, 'temperature_monitor_channels.csv')
    if os.path.exists(filename):
        header = 'temperature monitors channels selected\n'
        with open(filename, "w") as ap:
            ap.write(header)
            for i in range(len(channels)):
                thisVal = str(channels[i])
                ap.write( thisVal +'\n' )
            ap.close()


def read_intervals_file_csv():
    subdirpath = Path("Config")
    onlyfiles = [f for f in os.listdir(subdirpath)
                 if os.path.isfile(os.path.join(subdirpath, f))] 
    intervals_file = sorted([x for x in onlyfiles
                             if x.split(".csv")[0].endswith("temperature_monitor_intervals")])
    intervals = []
    # read the intervals file (if exists)
    if intervals_file:
        fn = os.path.join(subdirpath, intervals_file[0])
        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for i, line in enumerate(reader):
                if i>0 and line:
                    intervals.append(float(line[0]))
    else:
        intervals = [10.0, 13.3]
        header = 'temperature check interval (s), errors check interval (s)\n'
        filename = os.path.join(subdirpath, 'temperature_monitor_intervals.csv')
        with open(filename, "a") as ap:
            if (os.path.getsize(filename)) <= 0:
                ap.write(header)
                for i in range(len(intervals)):
                    thisVal = str(intervals[i])
                    ap.write( thisVal +'\n' )
                ap.close()
    return intervals

def write_intervals(intervals):
    subdirpath = Path("Config")
    filename = os.path.join(subdirpath, 'temperature_monitor_intervals.csv')
    if os.path.exists(filename):
        header = 'temperature check interval (s), errors check interval (s)\n'
        with open(filename, "w") as ap:
            ap.write(header)
            for i in range(len(intervals)):
                thisVal = str(intervals[i])
                ap.write( thisVal +'\n' )
            ap.close()


def read_thresholds_file_csv():
    subdirpath = Path("Config")
    onlyfiles = [f for f in os.listdir(subdirpath)
                 if os.path.isfile(os.path.join(subdirpath, f))] 
    thresholds_file = sorted([x for x in onlyfiles
                             if x.split(".csv")[0].endswith("temperature_monitor_thresholds")])
    thresholds = []
    # read the thresholds file (if exists)
    if thresholds_file:
        fn = os.path.join(subdirpath, thresholds_file[0])
        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for i, line in enumerate(reader):
                if i>0 and line:
                    thresholds.append([float(line[0]),float(line[1])])
    else: # define some defaults
        thresholds = [ 
                      [-270.15,-280.90],[-270.15,-280.90],[-270.15,-280.90],[-270.15,-280.90],
                      [-270.15,-280.90],[-270.15,-280.90],[-270.15,-280.90],[-270.15,-280.90],
                      [-270.15,-280.90],[-270.15,-280.90],[-270.15,-280.90],[-270.15,-280.90],
                      [-270.15,-280.90],[-270.15,-280.90],[-270.15,-280.90],[-270.15,-280.90]
                     ]
        header = 'lower threshold(deg. C), upper threshold (deg. C)\n'
        filename = os.path.join(subdirpath, 'temperature_monitor_thresholds.csv')
        with open(filename, "a") as ap:
            if (os.path.getsize(filename)) <= 0:
                ap.write(header)
                for i in range(len(thresholds)):
                    thisVal1 = str(thresholds[i][0])
                    thisVal2 = str(thresholds[i][1])
                    ap.write( thisVal1 + ',' + thisVal2 + '\n' )
                ap.close()
    return thresholds

def write_thresholds(thresholds):
    subdirpath = Path("Config")
    filename = os.path.join(subdirpath, 'temperature_monitor_thresholds.csv')
    if os.path.exists(filename):
        header = 'lower threshold(deg. C), upper threshold (deg. C)\n'
        with open(filename, "w") as ap:
            ap.write(header)
            for i in range(len(thresholds)):
                thisVal1 = str(thresholds[i][0])
                thisVal2 = str(thresholds[i][1])
                ap.write( thisVal1 + ',' + thisVal2 + '\n' )
            ap.close()


def read_ipaddresses_file_csv():
    subdirpath = Path("Config")
    onlyfiles = [f for f in os.listdir(subdirpath)
                 if os.path.isfile(os.path.join(subdirpath, f))] 
    ipaddresses_file = sorted([x for x in onlyfiles
                             if x.split(".csv")[0].endswith("temperature_monitor_ip_addresses")])
    ipaddresses = []
    # read the intervals file (if exists)
    if ipaddresses_file:
        fn = os.path.join(subdirpath, ipaddresses_file[0])
        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for i, line in enumerate(reader):
                if i>0 and line:
                    ipaddresses.append(line[0])
    else:
        ipaddresses = ['172.22.0.139','127.0.0.2','127.0.0.3','127.0.0.4']
        header = 'ip addresses of 4 Lake Shore Model 336 instruments\n'
        filename = os.path.join(subdirpath, 'temperature_monitor_ip_addresses.csv')
        with open(filename, "a") as ap:
            if (os.path.getsize(filename)) <= 0:
                ap.write(header)
                for i in range(len(ipaddresses)):
                    ap.write( ipaddresses[i] + '\n' )
                ap.close()
    return ipaddresses


def read_channel_labels_file_csv():
    subdirpath = Path("Config")
    onlyfiles = [f for f in os.listdir(subdirpath)
                 if os.path.isfile(os.path.join(subdirpath, f))] 
    labels_file = sorted([x for x in onlyfiles
                             if x.split(".csv")[0].endswith("temperature_monitor_channel_labels")])
    labels = []
    # read the channel labels file (if exists)
    if labels_file:
        fn = os.path.join(subdirpath, labels_file[0])
        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for i, line in enumerate(reader):
                if i>0 and line:
                    labels.append(line[0])
    else:
        labels = [ 'one', 'two', 'three', 'four',
                   'five', 'six', 'seven', 'eight',
                   'nine', 'ten', 'eleven', 'twelve',
                   'thirteen', 'fourteen', 'fifteen', 'sixteen' ]
        header = 'labels of the 16 temperature probe channels (4 from each Model 336)\n'
        filename = os.path.join(subdirpath, 'temperature_monitor_channel_labels.csv')
        with open(filename, "a") as ap:
            if (os.path.getsize(filename)) <= 0:
                ap.write(header)
                for i in range(len(labels)):
                    ap.write( labels[i] + '\n' )
                ap.close()
    return labels

##################################################################################################
class ConfigPanel:
    def __init__(self, to_tempController_queue):
        self.window = tk.Toplevel()
        self.to_tempController_queue = to_tempController_queue
        label = tk.Label(self.window, text="Choose channels to plot")

        # read the config files
        self.read_config_files()

        self.setup_gui()

    def setup_gui(self):
        n = ttk.Notebook(self.window)        
        f1 = ttk.Frame(n)
        f2 = ttk.Frame(n)
        f3 = ttk.Frame(n)
        n.add(f1, text='channels')
        n.add(f2, text='intervals')
        n.add(f3, text='thresholds')
        n.grid(row=1, column=0)

        ''' f1 - channels to plot '''
        sp0 = tk.Label(f1, height=1, text='')
        label_check1 = tk.Label(f1, text='1: A ')
        self.isChecked1 = tk.IntVar()
        self.check1 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked1,
                                      onvalue=1, offvalue=0)
        label_check2 = tk.Label(f1, text='1: B ')
        self.isChecked2 = tk.IntVar()
        self.check2 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked2,
                                      onvalue=1, offvalue=0)
        label_check3 = tk.Label(f1, text='1: C ')
        self.isChecked3 = tk.IntVar()
        self.check3 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked3,
                                      onvalue=1, offvalue=0)
        label_check4 = tk.Label(f1, text='1: D ')
        self.isChecked4 = tk.IntVar()
        self.check4 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked4,
                                      onvalue=1, offvalue=0)
 
        sp1 = tk.Label(f1, height=1, text='')
        label_check5 = tk.Label(f1, text='2: A ')
        self.isChecked5 = tk.IntVar()
        self.check5 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked5,
                                      onvalue=1, offvalue=0)
        label_check6 = tk.Label(f1, text='2: B ')
        self.isChecked6 = tk.IntVar()
        self.check6 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked6,
                                      onvalue=1, offvalue=0)
        label_check7 = tk.Label(f1, text='2: C ')
        self.isChecked7 = tk.IntVar()
        self.check7 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked7,
                                      onvalue=1, offvalue=0)
        label_check8 = tk.Label(f1, text='2: D ')
        self.isChecked8 = tk.IntVar()
        self.check8 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked8,
                                      onvalue=1, offvalue=0)
 
        sp2 = tk.Label(f1, height=1, text='')
        label_check9 = tk.Label(f1, text='3: A ')
        self.isChecked9 = tk.IntVar()
        self.check9 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked9,
                                      onvalue=1, offvalue=0)
        label_check10 = tk.Label(f1, text='3: B ')
        self.isChecked10 = tk.IntVar()
        self.check10 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked10,
                                      onvalue=1, offvalue=0)
        label_check11 = tk.Label(f1, text='3: C ')
        self.isChecked11 = tk.IntVar()
        self.check11 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked11,
                                      onvalue=1, offvalue=0)
        label_check12 = tk.Label(f1, text='3: D ')
        self.isChecked12 = tk.IntVar()
        self.check12 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked12,
                                      onvalue=1, offvalue=0)
 
        sp3 = tk.Label(f1, height=1, text='')
        label_check13 = tk.Label(f1, text='4: A ')
        self.isChecked13 = tk.IntVar()
        self.check13 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked13,
                                      onvalue=1, offvalue=0)
        label_check14 = tk.Label(f1, text='4: B ')
        self.isChecked14 = tk.IntVar()
        self.check14 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked14,
                                      onvalue=1, offvalue=0)
        label_check15 = tk.Label(f1, text='4: C ')
        self.isChecked15 = tk.IntVar()
        self.check15 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked15,
                                      onvalue=1, offvalue=0)
        label_check16 = tk.Label(f1, text='4: D ')
        self.isChecked16 = tk.IntVar()
        self.check16 = ttk.Checkbutton(f1, text='plot channel?', variable=self.isChecked16,
                                      onvalue=1, offvalue=0)
 
        sp4 = tk.Label(f1, height=1, text='')

        # select the channels as defined in the config file
        if self.selected:
            self.isChecked1.set(self.selected[0])
            self.isChecked2.set(self.selected[1])
            self.isChecked3.set(self.selected[2])
            self.isChecked4.set(self.selected[3])
            self.isChecked5.set(self.selected[4])
            self.isChecked6.set(self.selected[5])
            self.isChecked7.set(self.selected[6])
            self.isChecked8.set(self.selected[7])
            self.isChecked9.set(self.selected[8])
            self.isChecked10.set(self.selected[9])
            self.isChecked11.set(self.selected[10])
            self.isChecked12.set(self.selected[11])
            self.isChecked13.set(self.selected[12])
            self.isChecked14.set(self.selected[13])
            self.isChecked15.set(self.selected[14])
            self.isChecked16.set(self.selected[15])
            
        sp0.grid(row=0, column=0, columnspan=2, sticky='nswe')
        label_check1.grid(row=1, column=0, columnspan=1, sticky='nswe')
        self.check1.grid(row=1, column=1, columnspan=1, sticky='nswe')
        label_check2.grid(row=2, column=0, columnspan=1, sticky='nswe')
        self.check2.grid(row=2, column=1, columnspan=1, sticky='nswe')
        label_check3.grid(row=3, column=0, columnspan=1, sticky='nswe')
        self.check3.grid(row=3, column=1, columnspan=1, sticky='nswe')
        label_check4.grid(row=4, column=0, columnspan=1, sticky='nswe')
        self.check4.grid(row=4, column=1, columnspan=1, sticky='nswe')

        sp1.grid(row=5, column=0, columnspan=2, sticky='nswe')
        label_check5.grid(row=6, column=0, columnspan=1, sticky='nswe')
        self.check5.grid(row=6, column=1, columnspan=1, sticky='nswe')
        label_check6.grid(row=7, column=0, columnspan=1, sticky='nswe')
        self.check6.grid(row=7, column=1, columnspan=1, sticky='nswe')
        label_check7.grid(row=8, column=0, columnspan=1, sticky='nswe')
        self.check7.grid(row=8, column=1, columnspan=1, sticky='nswe')
        label_check8.grid(row=9, column=0, columnspan=1, sticky='nswe')
        self.check8.grid(row=9, column=1, columnspan=1, sticky='nswe')

        sp2.grid(row=10, column=0, columnspan=2, sticky='nswe')
        label_check9.grid(row=11, column=0, columnspan=1, sticky='nswe')
        self.check9.grid(row=11, column=1, columnspan=1, sticky='nswe')
        label_check10.grid(row=12, column=0, columnspan=1, sticky='nswe')
        self.check10.grid(row=12, column=1, columnspan=1, sticky='nswe')
        label_check11.grid(row=13, column=0, columnspan=1, sticky='nswe')
        self.check11.grid(row=13, column=1, columnspan=1, sticky='nswe')
        label_check12.grid(row=14, column=0, columnspan=1, sticky='nswe')
        self.check12.grid(row=14, column=1, columnspan=1, sticky='nswe')

        sp3.grid(row=15, column=0, columnspan=2, sticky='nswe')
        label_check13.grid(row=16, column=0, columnspan=1, sticky='nswe')
        self.check13.grid(row=16, column=1, columnspan=1, sticky='nswe')
        label_check14.grid(row=17, column=0, columnspan=1, sticky='nswe')
        self.check14.grid(row=17, column=1, columnspan=1, sticky='nswe')
        label_check15.grid(row=18, column=0, columnspan=1, sticky='nswe')
        self.check15.grid(row=18, column=1, columnspan=1, sticky='nswe')
        label_check16.grid(row=19, column=0, columnspan=1, sticky='nswe')
        self.check16.grid(row=19, column=1, columnspan=1, sticky='nswe')

        sp4.grid(row=20, column=0, columnspan=2, sticky='nswe')

        ''' f2 - update frequencies (temperatures, errors) '''
        spc_1 = tk.Label(f2, height=1, text=' ')
        tempText = 'The rate, in seconds,\nat which temperatures are\nchecked and ' + \
                   'messages sent\nto the Main program.'
        label_t_secs = tk.Label(f2, text=tempText)
        self.t_secs = tk.StringVar()
        self.t_secs.set(str(self.temps_secs))
        self.t_secs_entry = ttk.Entry(f2, width=8, textvariable=self.t_secs)

        errorsText = 'The rate, in seconds,\nat which the 336 opstat\nregister is ' + \
                     'checked for\nerrors, and any errors\nlogged in Data\n' + \
                     '/temperature_monitor_logs\n/errors.'
        spc_2 = tk.Label(f2, height=1, text=' ')
        label_e_secs = tk.Label(f2, text=errorsText)
        self.e_secs = tk.StringVar()
        self.e_secs.set(str(self.errors_secs))
        self.e_secs_entry = ttk.Entry(f2, width=8, textvariable=self.e_secs)
        spc_3 = tk.Label(f2, height=1, text=' ')

        spc_1.grid(row=0, column=0, columnspan=2, sticky='nswe')
        label_t_secs.grid(row=1, column=0, columnspan=1, sticky='nswe')
        self.t_secs_entry.grid(row=2, column=0, columnspan=1, sticky='nswe')
        spc_2.grid(row=3, column=0, columnspan=2, sticky='nswe')
        label_e_secs.grid(row=4, column=0, columnspan=1, sticky='nswe')
        self.e_secs_entry.grid(row=5, column=0, columnspan=1, sticky='nswe')
        
        ''' f3 - temperature channel thresholds '''
        spacer_1 = tk.Label(f3, height=1, text=' ')
        label_inst1a = tk.Label(f3, text='lower')
        label_inst1b = tk.Label(f3, text='upper')
        label_inst1 = tk.Label(f3, text='INSTRUMENT 1:')
        self.I1u1 = tk.StringVar()
        self.I1l1 = tk.StringVar()
        self.I1u1.set(str(self.thresholds[0][0]))
        self.I1l1.set(str(self.thresholds[0][1]))
        self.I1u1_entry = ttk.Entry(f3, width=12, textvariable=self.I1u1)
        self.I1l1_entry = ttk.Entry(f3, width=12, textvariable=self.I1l1)
        self.I1u2 = tk.StringVar()
        self.I1l2 = tk.StringVar()
        self.I1u2.set(str(self.thresholds[1][0]))
        self.I1l2.set(str(self.thresholds[1][1]))
        self.I1u2_entry = ttk.Entry(f3, width=12, textvariable=self.I1u2)
        self.I1l2_entry = ttk.Entry(f3, width=12, textvariable=self.I1l2)
        self.I1u3 = tk.StringVar()
        self.I1l3 = tk.StringVar()
        self.I1u3.set(str(self.thresholds[2][0]))
        self.I1l3.set(str(self.thresholds[2][1]))
        self.I1u3_entry = ttk.Entry(f3, width=12, textvariable=self.I1u3)
        self.I1l3_entry = ttk.Entry(f3, width=12, textvariable=self.I1l3)
        self.I1u4 = tk.StringVar()
        self.I1l4 = tk.StringVar()
        self.I1u4.set(str(self.thresholds[3][0]))
        self.I1l4.set(str(self.thresholds[3][1]))
        self.I1u4_entry = ttk.Entry(f3, width=12, textvariable=self.I1u4)
        self.I1l4_entry = ttk.Entry(f3, width=12, textvariable=self.I1l4)
        label_inst2 = tk.Label(f3, text='INSTRUMENT 2:')
        self.I2u1 = tk.StringVar()
        self.I2l1 = tk.StringVar()
        self.I2u1.set(str(self.thresholds[4][0]))
        self.I2l1.set(str(self.thresholds[4][1]))
        self.I2u1_entry = ttk.Entry(f3, width=12, textvariable=self.I2u1)
        self.I2l1_entry = ttk.Entry(f3, width=12, textvariable=self.I2l1)
        self.I2u2 = tk.StringVar()
        self.I2l2 = tk.StringVar()
        self.I2u2.set(str(self.thresholds[5][0]))
        self.I2l2.set(str(self.thresholds[5][1]))
        self.I2u2_entry = ttk.Entry(f3, width=12, textvariable=self.I2u2)
        self.I2l2_entry = ttk.Entry(f3, width=12, textvariable=self.I2l2)
        self.I2u3 = tk.StringVar()
        self.I2l3 = tk.StringVar()
        self.I2u3.set(str(self.thresholds[6][0]))
        self.I2l3.set(str(self.thresholds[6][1]))
        self.I2u3_entry = ttk.Entry(f3, width=12, textvariable=self.I2u3)
        self.I2l3_entry = ttk.Entry(f3, width=12, textvariable=self.I2l3)
        self.I2u4 = tk.StringVar()
        self.I2l4 = tk.StringVar()
        self.I2u4.set(str(self.thresholds[7][0]))
        self.I2l4.set(str(self.thresholds[7][1]))
        self.I2u4_entry = ttk.Entry(f3, width=12, textvariable=self.I2u4)
        self.I2l4_entry = ttk.Entry(f3, width=12, textvariable=self.I2l4)
        label_inst3 = tk.Label(f3, text='INSTRUMENT 3:')
        self.I3u1 = tk.StringVar()
        self.I3l1 = tk.StringVar()
        self.I3u1.set(str(self.thresholds[8][0]))
        self.I3l1.set(str(self.thresholds[8][1]))
        self.I3u1_entry = ttk.Entry(f3, width=12, textvariable=self.I3u1)
        self.I3l1_entry = ttk.Entry(f3, width=12, textvariable=self.I3l1)
        self.I3u2 = tk.StringVar()
        self.I3l2 = tk.StringVar()
        self.I3u2.set(str(self.thresholds[9][0]))
        self.I3l2.set(str(self.thresholds[9][1]))
        self.I3u2_entry = ttk.Entry(f3, width=12, textvariable=self.I3u2)
        self.I3l2_entry = ttk.Entry(f3, width=12, textvariable=self.I3l2)
        self.I3u3 = tk.StringVar()
        self.I3l3 = tk.StringVar()
        self.I3u3.set(str(self.thresholds[10][0]))
        self.I3l3.set(str(self.thresholds[10][1]))
        self.I3u3_entry = ttk.Entry(f3, width=12, textvariable=self.I3u3)
        self.I3l3_entry = ttk.Entry(f3, width=12, textvariable=self.I3l3)
        self.I3u4 = tk.StringVar()
        self.I3l4 = tk.StringVar()
        self.I3u4.set(str(self.thresholds[11][0]))
        self.I3l4.set(str(self.thresholds[11][1]))
        self.I3u4_entry = ttk.Entry(f3, width=12, textvariable=self.I3u4)
        self.I3l4_entry = ttk.Entry(f3, width=12, textvariable=self.I3l4)
        label_inst4 = tk.Label(f3, text='INSTRUMENT 4:')
        self.I4u1 = tk.StringVar()
        self.I4l1 = tk.StringVar()
        self.I4u1.set(str(self.thresholds[12][0]))
        self.I4l1.set(str(self.thresholds[12][1]))
        self.I4u1_entry = ttk.Entry(f3, width=12, textvariable=self.I4u1)
        self.I4l1_entry = ttk.Entry(f3, width=12, textvariable=self.I4l1)
        self.I4u2 = tk.StringVar()
        self.I4l2 = tk.StringVar()
        self.I4u2.set(str(self.thresholds[13][0]))
        self.I4l2.set(str(self.thresholds[13][1]))
        self.I4u2_entry = ttk.Entry(f3, width=12, textvariable=self.I4u2)
        self.I4l2_entry = ttk.Entry(f3, width=12, textvariable=self.I4l2)
        self.I4u3 = tk.StringVar()
        self.I4l3 = tk.StringVar()
        self.I4u3.set(str(self.thresholds[14][0]))
        self.I4l3.set(str(self.thresholds[14][1]))
        self.I4u3_entry = ttk.Entry(f3, width=12, textvariable=self.I4u3)
        self.I4l3_entry = ttk.Entry(f3, width=12, textvariable=self.I4l3)
        self.I4u4 = tk.StringVar()
        self.I4l4 = tk.StringVar()
        self.I4u4.set(str(self.thresholds[15][0]))
        self.I4l4.set(str(self.thresholds[15][1]))
        self.I4u4_entry = ttk.Entry(f3, width=12, textvariable=self.I4u4)
        self.I4l4_entry = ttk.Entry(f3, width=12, textvariable=self.I4l4)

        spacer_1.grid(row=0, column=0, columnspan=2, sticky='nswe')
        label_inst1a.grid(row=1, column=0, sticky='nswe')
        label_inst1b.grid(row=1, column=1, sticky='nswe')
        label_inst1.grid(row=2, column=0, sticky='nswe')
        self.I1u1_entry.grid(row=3, column=0, sticky='nswe')
        self.I1l1_entry.grid(row=3, column=1, sticky='nswe')
        self.I1u2_entry.grid(row=4, column=0, sticky='nswe')
        self.I1l2_entry.grid(row=4, column=1, sticky='nswe')
        self.I1u3_entry.grid(row=5, column=0, sticky='nswe')
        self.I1l3_entry.grid(row=5, column=1, sticky='nswe')
        self.I1u4_entry.grid(row=6, column=0, sticky='nswe')
        self.I1l4_entry.grid(row=6, column=1, sticky='nswe')
        label_inst2.grid(row=7, column=0, sticky='nswe')
        self.I2u1_entry.grid(row=8, column=0, sticky='nswe')
        self.I2l1_entry.grid(row=8, column=1, sticky='nswe')
        self.I2u2_entry.grid(row=9, column=0, sticky='nswe')
        self.I2l2_entry.grid(row=9, column=1, sticky='nswe')
        self.I2u3_entry.grid(row=10, column=0, sticky='nswe')
        self.I2l3_entry.grid(row=10, column=1, sticky='nswe')
        self.I2u4_entry.grid(row=11, column=0, sticky='nswe')
        self.I2l4_entry.grid(row=11, column=1, sticky='nswe')
        label_inst3.grid(row=12, column=0, sticky='nswe')
        self.I3u1_entry.grid(row=13, column=0, sticky='nswe')
        self.I3l1_entry.grid(row=13, column=1, sticky='nswe')
        self.I3u2_entry.grid(row=14, column=0, sticky='nswe')
        self.I3l2_entry.grid(row=14, column=1, sticky='nswe')
        self.I3u3_entry.grid(row=15, column=0, sticky='nswe')
        self.I3l3_entry.grid(row=15, column=1, sticky='nswe')
        self.I3u4_entry.grid(row=16, column=0, sticky='nswe')
        self.I3l4_entry.grid(row=16, column=1, sticky='nswe')
        label_inst4.grid(row=17, column=0, sticky='nswe')
        self.I4u1_entry.grid(row=18, column=0, sticky='nswe')
        self.I4l1_entry.grid(row=18, column=1, sticky='nswe')
        self.I4u2_entry.grid(row=19, column=0, sticky='nswe')
        self.I4l2_entry.grid(row=19, column=1, sticky='nswe')
        self.I4u3_entry.grid(row=20, column=0, sticky='nswe')
        self.I4l3_entry.grid(row=20, column=1, sticky='nswe')
        self.I4u4_entry.grid(row=21, column=0, sticky='nswe')
        self.I4l4_entry.grid(row=21, column=1, sticky='nswe')

        # close window button
        self.to_tempController_queue.put("TEMPERATURE_INTERVAL " + self.t_secs.get())
        time.sleep(0.3)
        self.to_tempController_queue.put("ERRORS_INTERVAL " + self.e_secs.get())
        button_apply_close = tk.Button(self.window, text="Apply & Close", command=self.apply_close) 
        button_apply_close.grid(row=2, column=0, sticky='s')

    def read_config_files(self):
        self.selected = read_channels_file_csv()
        self.temps_secs, self.errors_secs = read_intervals_file_csv()
        self.thresholds = read_thresholds_file_csv()
        self.ipaddresses = read_ipaddresses_file_csv()
    
    def apply_close(self):
        # write any changes to channels config files
        write_channels([ self.isChecked1.get(),self.isChecked2.get(),
                         self.isChecked3.get(),self.isChecked4.get(),
                         self.isChecked5.get(),self.isChecked6.get(),
                         self.isChecked7.get(),self.isChecked8.get(),
                         self.isChecked9.get(),self.isChecked10.get(),
                         self.isChecked11.get(),self.isChecked12.get(),
                         self.isChecked13.get(),self.isChecked14.get(),
                         self.isChecked15.get(),self.isChecked16.get() ])
        write_intervals([ self.t_secs.get(), self.e_secs.get() ])
        write_thresholds([ [self.I1u1.get(), self.I1l1.get()],
                           [self.I1u2.get(), self.I1l2.get()],
                           [self.I1u3.get(), self.I1l3.get()],
                           [self.I1u4.get(), self.I1l4.get()],
                           [self.I2u1.get(), self.I2l1.get()],
                           [self.I2u2.get(), self.I2l2.get()],
                           [self.I2u3.get(), self.I2l3.get()],
                           [self.I2u4.get(), self.I2l4.get()],
                           [self.I3u1.get(), self.I3l1.get()],
                           [self.I3u2.get(), self.I3l2.get()],
                           [self.I3u3.get(), self.I3l3.get()],
                           [self.I3u4.get(), self.I3l4.get()],
                           [self.I4u1.get(), self.I4l1.get()],
                           [self.I4u2.get(), self.I4l2.get()],
                           [self.I4u3.get(), self.I4l3.get()],
                           [self.I4u4.get(), self.I4l4.get()] ])
        self.read_config_files()
        self.setup_gui()
        # finally, close this window
        self.window.destroy()
    
##################################################################################################
class Tk_Handler():

    def __init__(self, root, canvas, line):
        self.root = root
        self.line = line
        self.to_tempController_queue = queue.Queue()
        self.from_tempController_queue = queue.Queue()
        self.from_errors_queue = queue.Queue()
        self.root.wm_title("Cryo-Vac Spectrometer Temperature Monitor")
        self.temperatureLogfile = temperatureLogfile()
        self.errorLogfile = errorLogfile()
        self.channels = read_channels_file_csv()
        self.intervals = read_intervals_file_csv()
        self.thresholds = read_thresholds_file_csv()
        self.ip_addresses = read_ipaddresses_file_csv()
        self.channel_labels = read_channel_labels_file_csv()

        '''
        Model 336 Temperature Controller (controls up to 4 Model 336 units)
        '''
        self.instrument = temperature_controller.LSModel336(LOGINTERVAL,
                                                            self.thresholds,
                                                            self.ip_addresses,
                                                            self.to_tempController_queue,
                                                            self.from_tempController_queue,
                                                            self.from_errors_queue)
        self.instrument.start()

        # Bottom button frame
        bottom_frame = tk.Frame(self.root)

        # min & max selection frame
        minmax_frame = tk.Frame(bottom_frame)

        # config and screengrab frame
        confgrab_frame = tk.Frame(bottom_frame)

        # minmax frame widgets 
        self.vmin = tk.IntVar()
        xmin_entry = tk.Entry(minmax_frame, textvariable=self.vmin, width=9, font='Calibri 11')
        xmin_button = tk.Button(minmax_frame, text='t min (sec)', width=8,
                                command=self._set_xmin)
        self.vmax = tk.IntVar()
        xmax_entry = tk.Entry(minmax_frame, textvariable=self.vmax, width=9, font='Calibri 11')
        xmax_button = tk.Button(minmax_frame, text='t max (sec)', width=8,
                                command=self._set_xmax)

        # confgrab frame widgets
        configure_button = tk.Button(confgrab_frame, text='configure', width=9,
                                     font='Calibri 9', fg="white", bg="blue",
                                     command=self._config_panel)
        saveplot_button = tk.Button(confgrab_frame, text='save plot', width=9,
                                    font='Calibri 9', fg="black", bg="orange",
                                    command=self._save_plot)

        # Full range scale button
        full_scale_button = tk.Button(bottom_frame, text="use full scale",\
                                    font=("Calibri", 11),\
                                    fg='black', bg='lightgray',\
                                    height=2, width=9, command=self._use_full_xscale)
        
        # Quit Button (place away from other buttons)
        quit_button = tk.Button(bottom_frame, text="Quit",\
                                font=("Calibri", 11),\
                                fg='white', bg='darkred',\
                                height=2, width=4, command=self._quit)
                                

        # Place the frames in a grid (row, column)
        tk.Grid.rowconfigure(self.root, 0, weight=1)
        tk.Grid.columnconfigure(self.root, 0, weight=1)
        canvas.get_tk_widget().grid(row=0, column=0, rowspan=1, columnspan=1, pady=2)
        bottom_frame.grid(row=1, column=0, rowspan=1, columnspan=5, pady=0)

        # Place the bottom widgets within a grid
        tk.Grid.rowconfigure(bottom_frame, 0, weight=1)
        tk.Grid.columnconfigure(bottom_frame, 0, weight=1)
        quit_button.grid(row=0, column=0, columnspan=1, padx=10, pady=0)
        full_scale_button.grid(row=0, column=1, columnspan=1, padx=10, pady=0)
        minmax_frame.grid(row=0, column=2, rowspan=2, columnspan=2)
        confgrab_frame.grid(row=0, column=4, rowspan=2, columnspan=1, padx=17)


        # Place the minmax widgets within a grid
        xmin_entry.grid(row=0, column=0, columnspan=1, padx=0, pady=0, sticky='we')
        xmin_button.grid(row=0, column=1, columnspan=1, padx=0, pady=0, sticky='we')
        xmax_entry.grid(row=1, column=0, columnspan=1, padx=0, pady=0, sticky='we')
        xmax_button.grid(row=1, column=1 , columnspan=1, padx=0, pady=0, sticky='we')

        # Place the confgrab widgets within a grid
        configure_button.grid(row=0, column=0, sticky='we')
        saveplot_button.grid(row=1, column=0, sticky='we')

        self.periodicCall()
        tk.mainloop()


    def _quit(self):
        self.instrument.stop() # terminate the temperature_controller thread
        self.instrument.join() # "
        self.instrument = None # "
        self.root.quit()       # stops mainloop
        self.root.destroy()    # "

    def periodicCall(self):
        """
        Check cryo-controller message queue every 100 ms.
        """
        self.refresh_plot()
        self.processTemperaturesIncoming()
        self.processErrorsIncoming()
        self.channels = read_channels_file_csv()
        self.root.after(100, self.periodicCall)

    def processTemperaturesIncoming(self):
        """Handle all messages currently in the cryo_queue, if any."""
        while self.from_tempController_queue.qsize():
            try:
                msg = self.from_tempController_queue.get()
                self.temperatureLogfile.append(msg)
                X = float(msg[0])
                A = float(msg[1])
                B = float(msg[2])
                C = float(msg[3])
                D = float(msg[4])
                self.plot_data(X, A, B, C, D)
            except queue.Empty:
                pass

    def processErrorsIncoming(self):
        while self.from_errors_queue.qsize():
            try:
                msg = self.from_errors_queue.get()
                msg = [ msg[0], " ".join(msg[1]), " ".join(msg[2]),\
                                " ".join(msg[3]), " ".join(msg[4]) ]
                #print("msg:", msg)
                self.errorLogfile.append(msg)
            except queue.Empty:
                pass

    def plot_data(self, X, A, B, C, D):
        self.line.x.append( X )
        self.line.ys[0].append( A )
        self.line.ys[1].append( B )
        self.line.ys[2].append( C )
        self.line.ys[3].append( D )
        self.line.channels = self.channels
        self.line.labels = self.channel_labels
        self.line.draw_plot()

    def refresh_plot(self):
        self.line.channels = self.channels
        self.line.draw_plot()

    def _set_xmin(self):
        self.line.set_xmin(self.vmin.get())

    def _set_xmax(self):
        self.line.set_xmax(self.vmax.get())

    def _use_full_xscale(self):
        self.line.set_full_xscale()

    def _config_panel(self):
        self.configPanel = ConfigPanel(self.to_tempController_queue)
        self.channels = self.configPanel.selected

    def _save_plot(self):
        self.line.save_plot()

##################################################################################################
root = tk.Tk()
MplMap.settings(root)
Tk_Handler(root, MplMap.get_canvas(), PlotLine())
