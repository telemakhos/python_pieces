# -*- coding: utf-8 -*-

__author__  = 'd. r. forester'
__date__    = '9 July 2019'
__version__ = '0.9.5'
__comment__ = 'V and S1 images used for oil detection'
'''
Detects oil on water in Pyxis LWIR imagery based on changes V and an S1-derived product.
Recharacterized water background based on Wasserstein distance between V_bkg and V images.

'''
import numpy as np
from skimage.filters import sobel
from scipy.stats import wasserstein_distance
from skimage.draw import polygon
from skimage.morphology import erosion, dilation, square
#from sklearn.neighbors import DistanceMetric
from sklearn.cluster import DBSCAN

from utils import dataFileInfo
from utils import custom_plots
from utils import clip_upper_lower

FILE_HEADER_SIZE = 2048

''' SETTINGS -----------------------------------------------------------------------------------'''
# v_file =  "../Data/deckPool/27_June/morning/V_sequence.raw"
# h_file =  "../Data/deckPool/27_June/morning/H_sequence.raw"
# t_file =  "../Data/deckPool/27_June/morning/Tc_sequence.raw"
# s1_file = "../Data/deckPool/27_June/morning/S1_sequence.raw"

# v_file =  "../Data/deckPool/27_June/afternoon/V_sequence.raw"
# h_file =  "../Data/deckPool/27_June/afternoon/H_sequence.raw"
# t_file =  "../Data/deckPool/27_June/afternoon/Tc_sequence.raw"
# s1_file = "../Data/deckPool/27_June/afternoon/S1_sequence.raw"

v_file =  "../Data/deckPool/12_July/V_sequence.raw"
h_file =  "../Data/deckPool/12_July/H_sequence.raw"
t_file =  "../Data/deckPool/12_July/Tc_sequence.raw"
s1_file = "../Data/deckPool/12_July/S1_sequence.raw"

outDir = "./output"

products_to_read = ('S1', 'T', 'V') #('S1','T','V','H')

DISPLAY = False
WRITE = False
PAUSE = False
VERBOSE = True
imgH, imgW = (512,640)

startFrame = 1 #4800 #4000 #1
next_frame_skip = 1 # skip this many between successive frames
nbNoms = 10
nbRead = 200 #8000 #6800 #8000


''' Read File Headers --------------------------------------------------------------------------'''

if 'S1' in products_to_read:
    S1_file_info = {}
    with open(s1_file, 'rb') as f:
        imgDataType, imgH, imgW, depth, frhsize, metasize, nbytes, totalFrames = dataFileInfo.getFileHeaderInfo(s1_file, False)
    S1_file_info['imgDataType'] = imgDataType
    S1_file_info['imgH'] = imgH
    S1_file_info['imgW'] = imgW
    S1_file_info['depth'] = depth
    S1_file_info['frhsize'] = frhsize
    S1_file_info['metasize'] = metasize
    S1_file_info['nbytes'] = nbytes
    S1_file_info['totalFrames'] = totalFrames

if 'T' in products_to_read:
    T_file_info = {}
    with open(t_file, 'rb') as f:
        imgDataType, imgH, imgW, depth, frhsize, metasize, nbytes, totalFrames = dataFileInfo.getFileHeaderInfo(t_file, False)
    T_file_info['imgDataType'] = imgDataType
    T_file_info['imgH'] = imgH
    T_file_info['imgW'] = imgW
    T_file_info['depth'] = depth
    T_file_info['frhsize'] = frhsize
    T_file_info['metasize'] = metasize
    T_file_info['nbytes'] = nbytes
    T_file_info['totalFrames'] = totalFrames

if 'V' in products_to_read:
    V_file_info = {}
    with open(v_file, 'rb') as f:
        imgDataType, imgH, imgW, depth, frhsize, metasize, nbytes, totalFrames = dataFileInfo.getFileHeaderInfo(v_file, False)
    V_file_info['imgDataType'] = imgDataType
    V_file_info['imgH'] = imgH
    V_file_info['imgW'] = imgW
    V_file_info['depth'] = depth
    V_file_info['frhsize'] = frhsize
    V_file_info['metasize'] = metasize
    V_file_info['nbytes'] = nbytes
    V_file_info['totalFrames'] = totalFrames

if 'H' in products_to_read:
    H_file_info = {}
    with open(h_file, 'rb') as f:
        imgDataType, imgH, imgW, depth, frhsize, metasize, nbytes, totalFrames = dataFileInfo.getFileHeaderInfo(h_file, False)
    H_file_info['imgDataType'] = imgDataType
    H_file_info['imgH'] = imgH
    H_file_info['imgW'] = imgW
    H_file_info['depth'] = depth
    H_file_info['frhsize'] = frhsize
    H_file_info['metasize'] = metasize
    H_file_info['nbytes'] = nbytes
    H_file_info['totalFrames'] = totalFrames


total_frames = V_file_info['totalFrames']
print('Total Frames:', V_file_info['totalFrames'])

''' Define masks --------------------------------------------------------------------------------'''
# a mask for the pool
mask = np.zeros((imgH, imgW), np.uint8)
poly_vertices = np.array( ((260,38),(119,206),(163,494),(334,391)) ) # exact crop
#poly_vertices = np.array( ((266,8),(109,196),(153,514),(344,401)) )   # shows some pool edge
#poly_vertices = np.array( ((256,39),(117,208),(152,433),(152,433),(187,413),(200,471),(327,388)) ) # remove oil pan entirely
rr, cc = polygon(poly_vertices[:,0], poly_vertices[:,1], mask.shape)
mask[rr,cc] = 1
#custom_plots.show_img_return_input(mask, 'mask', cm='gray', ask=True)

# a mask for the oil pan inside the pool
panMask = np.zeros((imgH, imgW), np.uint8)
poly_vertices = np.array( ((183,425),(155,445),(160,492),(194,476)) )
#poly_vertices = np.array( ((146,440),(187,412),(199,477),(153,504)) ) # mask a slightly larger area
rr, cc = polygon(poly_vertices[:,0], poly_vertices[:,1], mask.shape)
panMask[rr,cc] = 1
pidx = np.where(panMask==1)
#custom_plots.show_img_return_input(panMask, 'panMask', cm='gray', ask=True)

# a mask for getting a mean value from the center of the pool (for filling-in oil-pan; for TESTING only)
centerMask = np.zeros((imgH, imgW), np.uint8)
poly_vertices = np.array( ((242,236),(182,236),(182,335),(242,335)) )
rr, cc = polygon(poly_vertices[:,0], poly_vertices[:,1], mask.shape)
centerMask[rr,cc] = 1
#custom_plots.show_img_return_input(centerMask, 'centerMask', cm='gray', ask=True)



''' GRAB FRAMES (or read frame-by-frame from a file)------------------------------------------'''
v_wass_dist_thresh = 100
s1_wass_dist_thresh = 50
iCollect = 0
iSinceBkg = 0
CHARACTERIZE = True   # initialize
APPLY_PAN_MASK = False # mask out oil pan from background frame collections
EDGE_MASK = True    # use the first V background images to create a mask to remove container edges
wdVPc = 0.0
wdSPc = 0.0
wass_floats = []
S1noms = []
Vnoms = []
Tnoms = []

#dist = DistanceMetric.get_metric('manhattan')

rechar_frame_numbers = []
V_Delta = np.zeros((imgH, imgW), np.float32)
VD_Accumulator = np.zeros((imgH, imgW), np.float64)
nbConnected = 0
compareDists = True # initialize flag
ii = startFrame # the image lists index


if 'S1' in products_to_read:
    fs1 = open(s1_file, 'rb')
    s1offset = FILE_HEADER_SIZE
    s1offset += (startFrame -1) * (S1_file_info['frhsize'] + S1_file_info['metasize'] + 
                (S1_file_info['imgH'] * S1_file_info['imgW'] * S1_file_info['depth'] * S1_file_info['nbytes']))

if 'T' in products_to_read:
    ft = open(t_file, 'rb')
    toffset = FILE_HEADER_SIZE
    toffset += (startFrame -1) * (T_file_info['frhsize'] + T_file_info['metasize'] + 
                (T_file_info['imgH'] * T_file_info['imgW'] * T_file_info['depth'] * T_file_info['nbytes']))

if 'V' in products_to_read:
    fv = open(v_file, 'rb')
    voffset = FILE_HEADER_SIZE
    voffset += (startFrame -1) * (V_file_info['frhsize'] + V_file_info['metasize'] + 
                (V_file_info['imgH'] * V_file_info['imgW'] * V_file_info['depth'] * V_file_info['nbytes']))

if 'H' in products_to_read:
    fh = open(h_file, 'rb')
    hoffset = FILE_HEADER_SIZE
    hoffset += (startFrame -1) * (H_file_info['frhsize'] + H_file_info['metasize'] + 
                (H_file_info['imgH'] * H_file_info['imgW'] * H_file_info['depth'] * H_file_info['nbytes']))

while (ii < total_frames):
#while(ii < startFrame + nbRead):
    imgnumb = str(ii).zfill(4)
    
    
    ''' S1 '''
    if 'S1' in products_to_read:
        
        if S1_file_info['frhsize'] > 0:
            frmHdr = np.memmap(fs1, dtype='uint8', mode='r', offset=s1offset, shape=S1_file_info['frhsize']).tolist()
            s1_frameHdr = str(bytes(frmHdr), 'utf-8')
            tsList = s1_frameHdr.split("=")[-1].split("_")
            tsList[-1] = tsList[-1].split(".")[0]
            TimeStamp = ( tsList[0] + "/" + tsList[1] + "/" + tsList[2] + "   " +
                          tsList[3] + ":" + tsList[4] + ":" + tsList[5] )
            print(TimeStamp)
            s1offset += S1_file_info['frhsize']
            
        if S1_file_info['metasize'] >0:
            metaHdr = np.memmap(fs1, dtype='uint8', mode='r', offset=s1offset, shape=S1_file_info['metasize']).tolist()
            s1_metaHdr = str(bytes(metaHdr), 'utf-8')
            s1offset += S1_file_info['metasize']
            
        s1frame = np.memmap(fs1, dtype=S1_file_info['imgDataType'], mode='r', offset=s1offset,
                            shape=(S1_file_info['imgH']*S1_file_info['imgW']*S1_file_info['depth']))
        if S1_file_info['depth'] > 1:
            s1frame = s1frame.reshape(S1_file_info['imgH'], S1_file_info['imgW'], S1_file_info['depth'])
        else:
            s1frame = s1frame.reshape(S1_file_info['imgH'], S1_file_info['imgW'])
        s1offset += S1_file_info['imgH'] * S1_file_info['imgW'] * S1_file_info['depth'] * S1_file_info['nbytes']
        S1 = s1frame.copy()
        #custom_plots.show_img_return_input(S1, 'S1 - '+imgnumb, cm='gray', ask=False)
        
        
    ''' T '''
    if 'T' in products_to_read:
        
        if T_file_info['frhsize'] > 0:
            frmHdr = np.memmap(ft, dtype='uint8', mode='r', offset=toffset, shape=T_file_info['frhsize']).tolist()
            t_frameHdr = str(bytes(frmHdr), 'utf-8')
            toffset += T_file_info['frhsize']
        
        if T_file_info['metasize'] >0:
            metaHdr = np.memmap(ft, dtype='uint8', mode='r', offset=toffset, shape=T_file_info['metasize']).tolist()
            t_metaHdr = str(bytes(metaHdr), 'utf-8')
            toffset += T_file_info['metasize']
        
        tframe = np.memmap(ft, dtype=T_file_info['imgDataType'], mode='r', offset=toffset,
                           shape=(T_file_info['imgH']*T_file_info['imgW']*T_file_info['depth']))
        if T_file_info['depth'] > 1:
            tframe = tframe.reshape(T_file_info['imgH'], T_file_info['imgW'], T_file_info['depth'])
        else:
            tframe = tframe.reshape(T_file_info['imgH'], T_file_info['imgW'])
        toffset += T_file_info['imgH'] * T_file_info['imgW'] * T_file_info['depth'] * T_file_info['nbytes']
        Timg = tframe.copy()
        #custom_plots.show_img_return_input(Timg, 'Timg - '+imgnumb, cm='gray', ask=True)
    
    
    ''' V '''
    if 'V' in products_to_read:
        
        if V_file_info['frhsize'] > 0:
            frmHdr = np.memmap(fv, dtype='uint8', mode='r', offset=voffset, shape=V_file_info['frhsize']).tolist()
            v_frameHdr = str(bytes(frmHdr), 'utf-8')
            voffset += V_file_info['frhsize']
            
        if V_file_info['metasize'] >0:
            metaHdr = np.memmap(fV, dtype='uint8', mode='r', offset=voffset, shape=V_file_info['metasize']).tolist()
            v_metaHdr = str(bytes(metaHdr), 'utf-8')
            voffset += V_file_info['metasize']
            
        vframe = np.memmap(fv, dtype=V_file_info['imgDataType'], mode='r', offset=voffset,
                           shape=(V_file_info['imgH']*V_file_info['imgW']*V_file_info['depth']))
        if V_file_info['depth'] > 1:
            vframe = vframe.reshape(V_file_info['imgH'], V_file_info['imgW'], V_file_info['depth'])
        else:
            vframe = vframe.reshape(V_file_info['imgH'], V_file_info['imgW'])
        voffset += V_file_info['imgH'] * V_file_info['imgW'] * V_file_info['depth'] * V_file_info['nbytes']
        Vimg = vframe.copy()
        #custom_plots.show_img_return_input(Vimg, 'Vimg - '+imgnumb, cm='gray', ask=True)
    

    ''' H '''
    if 'H' in products_to_read:
        
        if H_file_info['frhsize'] > 0:
            frmHdr = np.memmap(fh, dtype='uint8', mode='r', offset=hoffset, shape=H_file_info['frhsize']).tolist()
            h_frameHdr = str(bytes(frmHdr), 'utf-8')
            hoffset += H_file_info['frhsize']
            
        if H_file_info['metasize'] >0:
            metaHdr = np.memmap(fh, dtype='uint8', mode='r', offset=hoffset, shape=H_file_info['metasize']).tolist()
            h_metaHdr = str(bytes(metaHdr), 'utf-8')
            hoffset += H_file_info['metasize']
            
        hframe = np.memmap(fh, dtype=H_file_info['imgDataType'], mode='r', offset=hoffset,
                           shape=(H_file_info['imgH']*H_file_info['imgW']*H_file_info['depth']))
        if H_file_info['depth'] > 1:
            hframe = hframe.reshape(H_file_info['imgH'], H_file_info['imgW'], H_file_info['depth'])
        else:
            hframe = hframe.reshape(H_file_info['imgH'], H_file_info['imgW'])
        hoffset += H_file_info['imgH'] * H_file_info['imgW'] * H_file_info['depth'] * H_file_info['nbytes']
        Himg = hframe.copy()
        #custom_plots.show_img_return_input(Himg, 'Himg - '+imgnumb, cm='gray', ask=True)
    
    
    
    
    
    if CHARACTERIZE:
        if iCollect < nbNoms:
            print("Collecting new baseline ... frame", iCollect)
            if 'T' in products_to_read:
                Tnoms.append(Timg)
            if 'V' in products_to_read:
                Vnoms.append(Vimg)
            if 'H' in products_to_read:
                Hnoms.append(Himg)
            if 'S1' in products_to_read:
                S1noms.append(S1)
            iCollect += 1
        else:
            print("Calculating background values ...")
            
            # T
            # Tnom = np.array(Tnoms)
            # Tnom = np.mean(Tnom, axis=0)
            
            # V
            Vnom = np.array(Vnoms)       
            Vnom = np.mean(Vnoms, axis=0)
            Vnom = Vnom * mask
            #if ...Vnom[Vnom>0]
            vnommean = np.mean(Vnom[Vnom>0])
            Vnom /= vnommean
            #custom_plots.show_img_return_input(Vnom, 'Vnom - '+imgnumb, cm='gray', ask=True)
            if EDGE_MASK:
                maskedImg = Vnom*mask / np.max(Vnom)
                maskedImg = sobel(maskedImg)
                maskedImg /= np.max(maskedImg)
                maskedImg[maskedImg < 0.1] = 0
                #custom_plots.show_img_return_input(maskedImg, 'maskedImg1 - '+imgnumb, cm='gray', ask=True)
                maskedImg = dilation(maskedImg, square(3))
                #custom_plots.show_img_return_input(maskedImg, 'maskedImg2 - '+imgnumb, cm='gray', ask=True)
                maskedImg[maskedImg==1] = 0
                maskedImg[maskedImg==0] = 1
                maskedImg[maskedImg<1] = 0
                #custom_plots.show_img_return_input(maskedImg, 'maskedImg3 - '+imgnumb, cm='gray', ask=True)
                maskedImg *= mask
                #custom_plots.show_img_return_input(maskedImg, 'maskedImg4 - '+imgnumb, cm='gray', ask=True)
                mask = maskedImg
                #EDGE_MASK = False # only find tmask on first background collection             
            
            # S1
            S1nom = np.array(S1noms)
            S1nom = np.mean(S1nom, axis=0)
            S1nom = clip_upper_lower.clip(S1nom, lp=0.0, up=99.0)
            #s1nomabsmean = np.abs(np.mean(S1nom))
            s1nom_mean = np.mean(S1nom[S1nom*mask>0])          
            S1nom[S1nom>0] = s1nom_mean
            S1nom += 2*np.abs(S1nom)
            #custom_plots.show_img_return_input(S1nom, 'S1nom - '+imgnumb, cm='gray', ask=True)
            
            iSinceBkg = 0
            CHARACTERIZE = False
            compareDists = False
    
        
    
    else: # perform detection in current frame
    
        # T
        Timg = clip_upper_lower.clip(Timg, lp=0.0, up=99.0)
        timg = Timg.copy()
        
        # V
        #Vimg = clip_upper_lower.clip(Vimg, lp=0.0, up=99.0)
        Vimg = Vimg*mask
        Vimg /= vnommean
        vimg = Vimg.copy()
        #custom_plots.show_img_return_input(Vimg, 'V - '+imgnumb, cm='gray', ask=True)
        
        # S1
        S1[S1>0] = s1nom_mean
        #custom_plots.show_img_return_input(S1, 'S1 - '+imgnumb, cm='gray', ask=False)
        S1 += 2*np.abs(S1)
        s1img = S1.copy()*mask      
        #custom_plots.show_img_return_input(S1, 'S1 - '+imgnumb, cm='gray', ask=False)
        
        # V Delta
        V_Delta = (Vimg - Vnom) / Vnom
        V_Delta *= mask
        V_Delta = clip_upper_lower.clip(V_Delta, lp=0.0, up=99.0)
        #V_Delta /= np.mean(V_Delta[V_Delta>0])
        if V_Delta[V_Delta>0].shape[0] > 0:
            V_Delta /= np.mean(V_Delta[V_Delta>0])

        #custom_plots.show_img_return_input(V_Delta, 'V_Delta - '+imgnumb, cm='gray', ask=True)
        #print("mean V_Delta:", np.mean(V_Delta))
        #nnpctile = np.percentile(V_Delta, 99.9)
        #print("nnpctile: ", nnpctile)
        #V_Delta[V_Delta<nnpctile] = 0
        vdimg = V_Delta.copy()
        #custom_plots.show_img_return_input(V_Delta, 'V_Delta - '+imgnumb, cm='gray', ask=True)
        
        VD_Accumulator += V_Delta
        custom_plots.show_img_return_input(VD_Accumulator, 'VD_Accumulator - '+imgnumb, cm='gray', ask=False)
        #custom_plots.write_img(VD_Accumulator, imgnumb, "VDelta Accumulator  #"+imgnumb+"    "+TimeStamp, outDir+'/vdelta_accumulator')
        
        
        # S1 Delta
        S1_Delta = (S1 - S1nom) / S1nom
        S1_Delta *= mask
        S1_Delta = clip_upper_lower.clip(S1_Delta, lp=0.0, up=99.0)
        S1_Delta[S1_Delta>0] = 0
        S1_Delta = np.abs(S1_Delta)
        #S1_Delta = np.abs(S1_Delta)
        #custom_plots.show_img_return_input(S1_Delta, 'S1_Delta - '+imgnumb, cm='gray', ask=True)
        #nnpctile = np.percentile(S1_Delta, 90)
        #S1_Delta[S1_Delta<nnpctile] = 0
        #S1_Delta = erosion(S1_Delta, square(3))
        #gtzidx = np.where(S1_Delta>0.01)
        #avgval = np.mean(S1_Delta[gtzidx])
        #print(avgval)
        #S1_Delta[S1_Delta<avgval] = 0
        s1dimg = S1_Delta.copy()
        #custom_plots.show_img_return_input(S1_Delta, 'S1_Delta - '+imgnumb, cm='gray', ask=False)

        
        # clImg = V_Delta.copy()/np.max(V_Delta)
        # clImg[clImg<=0.1] = 0
        # #clImg[clImg>0.1] = 1
        # idx = np.where(clImg>0)
        # X = np.array([[idx[0][ii], idx[1][ii]] for ii in range(len(idx[0]))])
        # #print("X:", X)
        # clustering = DBSCAN(eps=2, min_samples=10).fit(X)
        # #print("clustering.labels_:", clustering.labels_)
        # unique_labels = [x for x in set(clustering.labels_) if x!=-1]
        # #print("unique_labels:", unique_labels)
        # label_lengths = [len([x for x in clustering.labels_ if x==ul]) for ul in unique_labels]
        # #print("label_lengths:", label_lengths)
        # max_len = max(label_lengths)
        # #print("max_len:", max_len)
        # max_idx = [ii for ii in range(len(label_lengths)) if (
        #            label_lengths[ii]==max_len and max_len>300)] # define a minimum cluster size in number of pixels
        # if max_idx:
        #     print("identified cluster(s) ...")
        #     max_label = unique_labels[max_idx[0]]
        #     #print("max_label:", max_label)
        #     indexes = [ii for ii in range(len(X)) if clustering.labels_[ii]==max_label]
        #     ys = [ X[i][0] for i in indexes ]
        #     xs = [ X[i][1] for i in indexes ]
        #     # Display only
        #     clImg[[ys,xs]] = 2
        #     clImg[clImg<2] = 0
        #     #custom_plots.show_img_return_input(clImg, 'clImg - '+imgnumb, cm='gray', ask=True)
        # 
        #     # filter "sparkles" from V_Delta
        #     V_Delta = np.zeros_like(V_Delta)
        #     V_Delta[[ys,xs]] = 1
        #     vdimg = V_Delta.copy()
        
        
        # Combined V_Delta and S1_Delta
        Combined = np.zeros_like(mask)
        Combined[(S1_Delta>0) & (V_Delta>0)] = 1
        #Combined = V_Delta
        Combined[Combined<0] = 0
        Combined = dilation(Combined, square(3))
        #custom_plots.show_img_return_input(Combined, 'Combined - '+imgnumb, cm='gray', ask=False)
        
        
        
        if (WRITE or DISPLAY):
            # Plotting ##########################################################
            Trgb = np.zeros((imgH,imgW,3), np.uint8)
            timg -= np.min(timg)
            timgmax = np.max(timg)
            if timgmax > 0:
                timg /= timgmax
            timg *= 255
            timg = clip_upper_lower.clip(timg, lp=0.0, up=99.0)
            Trgb[:,:,0] = timg
            Trgb[:,:,1] = timg
            Trgb[:,:,2] = timg         
        
            # Vrgb = np.zeros((imgH,imgW,3), np.uint8)
            # vimg -= np.min(vimg)
            # vimgmax = np.max(vimg)
            # if vimgmax > 0:
            #     vimg /= vimgmax
            # vimg *= 255
            # vimg = clip_upper_lower.clip(vimg, lp=0.0, up=99.0)
            # vimg *= mask
            # Vrgb[:,:,0] = vimg
            # Vrgb[:,:,1] = vimg
            # Vrgb[:,:,2] = vimg
        
            VDrgb = np.zeros((imgH,imgW,3), np.uint8)
            vdimg -= np.min(vdimg)
            vdimgmax = np.max(vdimg)
            if vdimgmax > 0:
                vdimg /= vdimgmax
            vdimg *= 255
            vdimg = clip_upper_lower.clip(vdimg, lp=0.0, up=99.0)
            vdimg *= mask
            VDrgb[:,:,0] = vdimg
            VDrgb[:,:,1] = vdimg
            VDrgb[:,:,2] = vdimg               
        
            # S1rgb = np.zeros((imgH,imgW,3), np.uint8)
            # s1img -= np.min(s1img)
            # s1imgmax = np.max(s1img)
            # if s1imgmax > 0:
            #     s1img /= s1imgmax
            # s1img *= 255
            # s1img = clip_upper_lower.clip(s1img, lp=0.0, up=99.0)
            # S1rgb[:,:,0] = s1img
            # S1rgb[:,:,1] = s1img
            # S1rgb[:,:,2] = s1img
        
            S1Drgb = np.zeros((imgH,imgW,3), np.uint8)
            s1dimg -= np.min(s1dimg)
            s1dimgmax = np.max(s1dimg)
            if s1dimgmax > 0:
                s1dimg /= s1dimgmax
            s1dimg *= 255
            s1dimg = clip_upper_lower.clip(s1dimg, lp=0.0, up=99.0)
            S1Drgb[:,:,0] = s1dimg
            S1Drgb[:,:,1] = s1dimg
            S1Drgb[:,:,2] = s1dimg        
        
            # 1 X 1 Plot
            Drgb = Trgb.copy()
            Drgb[:,:,0][Combined>0.005] = 255 # color the detections in red
            #custom_plots.show_img_return_input(Drgb, imgnumb, ask=False)
            #custom_plots.write_img(Drgb, imgnumb, outDir+'/detections')
        
            # # 1 X 2 Plot
            # two = np.zeros((imgH, 2*imgW, 3), np.uint8)
            # two[:,0:imgW,:] = S1rgb
            # two[:,imgW:,:]  = S1Drgb
            # #custom_plots.show_img_return_input(two, TimeStamp, ask=False)
            # custom_plots.write_img(two, imgnumb, '', outDir+'/S1_S1delta')
        
            # # 1 X 3 Plot
            # three = np.zeros((imgH, 3*imgW, 3), np.uint8)
            # three[:,0:imgW,:] = Vrgb
            # three[:,imgW:2*imgW,:]  = S1rgb
            # three[:,2*imgW:3*imgW,:]  = S1Drgb
            # #custom_plots.show_img_return_input(three, TimeStamp, ask=False)
            # custom_plots.write_img(three, imgnumb, '', outDir+'/v_s1p_s1d')      
        
            # 1 X 3 Plot
            three = np.zeros((imgH, 3*imgW, 3), np.uint8)
            three[:,0:imgW,:] = VDrgb
            three[:,imgW:2*imgW,:]  = S1Drgb
            three[:,2*imgW:3*imgW,:]  = Drgb
            # draw borders separating images
            three[:,imgW,0] = 244
            three[:,imgW,1] = 232
            three[:,imgW,2] = 104
            three[:,2*imgW,0] = 244
            three[:,2*imgW,1] = 232
            three[:,2*imgW,2] = 104
            
            if WRITE:
                custom_plots.write_img(three, imgnumb, "#"+imgnumb+"    "+TimeStamp, outDir+'/detections_afternoon_rechar_wass_thresh_20')
            if DISPLAY:
                custom_plots.show_img_return_input(three, "#"+imgnumb+"    "+TimeStamp, ask=False)
        
            #####################################################################
        
        
        
        # # Calculate Wasserstein Distance for Vimg and Vnom
        # img2absmean = np.abs(np.mean(Vimg))
        # img1 = (Vnom/vnomabsmean)*mask
        # img1 = img1.reshape(imgH*imgW)
        # img2 = (Vimg/img2absmean)*mask
        # img2 = img2.reshape(imgH*imgW)
        # wdv = wasserstein_distance(img2, img1)
        # print(imgnumb, "  V   wasserstein_distance: {:.{}f}".format(wdv,2))        
        # 
        # # Has the abs wass dist changed a measurable amount?
        # if (abs(wdv) > 0):# and (abs(wds) > 0):
        # 
        #     if (iSinceBkg == 0):
        #         wd0v = wdv
        #         #wd0s = wds
        # 
        #     wdVPc = np.abs(((wdv - wd0v)/wd0v)*100)
        #     #wdSPc = np.abs(((wds - wd0s)/wd0s)*100)
        # 
        #     iSinceBkg += 1
        # 
        #     print("V  Absolute Wasserstein Distance percent change: {:.{}f}".format(wdVPc,2))
        #     #print("S1 Absolute Wasserstein Distance percent change: {:.{}f}".format(wdSPc,2))
        #     #wass_floats.append(wdVPc)
        # 
        #     # It is permissible to recharacterize the background using V_Delta,
        #     # as long as no detection is made in S1_Delta
        #     IS_PERMISSIBLE = True
        #     #if ((wdVPc > v_wass_dist_thresh) or (wdSPc > s_wass_dist_thresh)) and IS_PERMISSIBLE:
        #     if (wdVPc > v_wass_dist_thresh) and IS_PERMISSIBLE:
        #         print("\nCollecting new frames for recharacterization of water .....\n")
        #         CHARACTERIZE = True
        #         iCollect = 0
        #         S1noms = []
        #         Vnoms = []
        #         Tnoms = []
        #         rechar_frame_numbers.append(imgnumb + "   " + TimeStamp)
        # 
        # else:
        #     print("no measurable change in wdVPc.")
              

    ii += next_frame_skip
    
# Close the files
if 'S1' in products_to_read:    
    fs1.close()
if 'T' in products_to_read:    
    ft.close()
if 'V' in products_to_read:    
    fv.close()
if 'H' in products_to_read:    
    fh.close()

print( 'rechar_frame_numbers:', rechar_frame_numbers )

# # Abs. Wasserstein Distance vs. Frame Number
# import matplotlib.pyplot as plt
# x = np.arange(len(wass_floats))
# fig, ax = plt.subplots()
# ax.plot(x, np.abs(wass_floats), '-', color='blue', label = 'Wasserstein distance from V and V_bkg (thresh = 100)')
# legend = ax.legend(loc='upper left', shadow=True)
# plt.title('Wasserstein distance vs frame number')
# plt.show()

