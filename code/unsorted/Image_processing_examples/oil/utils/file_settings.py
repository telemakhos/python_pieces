'''
file settings when reading frames from a file
this script is not used when processesing live frames
'''

import numpy as np

def get_settings(file_designation, imgH, imgW):
    
    mask = np.ones((imgH,imgW), np.uint16)
    y1,y2 = (0,imgH) # default
    x1,x2 = (0,imgW) # default
        
    ''' FILE A '''
    if file_designation == 'A':
        file_desc = "two pans in square tank"
        inFile = "../Data/ohmsett/IR__2016_07_26_15_21_41_1-9382.s1"
        nbSkipNom = 3650 # skip past this number of frames to first frame of interest
        nbSkip = 0 # skip past this number of frames to first frame of interest
        next_frame_skip = 1 # skip this many between successive frames
        nbNoms = 200
        nbRead = 3000
        
        # define mask
        y1,y2 = (257,384)
        x1,x2 = (177,431)
        
        
    # ''' FILE AA '''
    # if file_designation == 'AA':
    #     file_desc = "several pans in square tank"
    #     inFile = "../Data/ohmsett/IR__04_16_57_1-23095.s1" # 072516_43Deg_Diurnal
    #     nbSkipNom = 1800 #0 # skip past this number of frames to first frame of interest
    #     nbSkip = 0 # skip past this number of frames to first frame of interest
    #     next_frame_skip = 30 # skip this many between successive frames
    #     nbNoms = 100 #500
    #     nbRead = 12000 #22000
    # 
    #     # define mask
    #     y1,y2 = (160,336)
    #     x1,x2 = (160,432)
        
        
    ''' FILE AA '''
    if file_designation == 'AA':
        file_desc = "several pans in square tank"
        inFile = "../Data/ohmsett/IR__04_16_57_subset/S1_sequence.raw" # 072516_43Deg_Diurnal
        nbSkipNom = 0 # skip past this number of frames to first frame of interest
        nbSkip = 0 # skip past this number of frames to first frame of interest
        next_frame_skip = 10 # skip this many between successive frames
        nbNoms = 10 #500
        nbRead = 800
        
        # define mask
        y1,y2 = (160,336)
        x1,x2 = (160,432)
        
        
    ''' FILE BB '''
    if file_designation == 'BB':
        file_desc = "parking deck pool with 2 oil pans"
        inFile = "../Data/deckPool/12_July/S1_sequence.raw"
        nbSkipNom = 1000 # skip past this number of frames to first frame of interest
        nbSkip = 0 # skip past this number of frames to first frame of interest
        next_frame_skip = 1 # skip this many between successive frames
        nbNoms = 10 #500
        nbRead = 1000 - (10 + 500)
        
        # define mask
        y1,y2 = (160,336)
        x1,x2 = (160,432)   
                  
        
        
    ''' FILE B '''
    if file_designation == 'B':
        file_desc = "oil blobs moving away from sensor"
        inFile = "../Data/ohmsett/IR__2016_07_29_09_16_42_1-1315.s1"
        nbSkipNom = 0 # skip past this number of frames to first frame of interest
        nbSkip = 0 # skip past this number of frames to first frame of interest
        next_frame_skip = 1 # skip this many between successive frames
        nbNoms = 20
        nbRead = 1200
        
        # no mask necessary
        
        
    ''' FILE C '''
    if file_designation == 'C':
        file_desc = "edges of pool"
        inFile = "../Data/ohmsett/IR__2016_07_28_10_27_59_1-862.s1"
        nbSkipNom = 0 # skip past this number of frames to first frame of interest
        nbSkip = 0 # skip past this number of frames to first frame of interest
        next_frame_skip = 1 # skip this many between successive frames
        nbNoms = 100
        nbRead = 750
        
        # define mask
        y1,y2 = (64,400)
        x1,x2 = (0,545)
        
        
    ''' FILE D '''
    if file_designation == 'D':
        file_desc = "oil dish in pan in lab"
        inFile = "../Data/old/2018_10_05_12_47_45_1_9645.s1"
        nbSkipNom = 650 # skip past this number of frames to first frame of interest
        nbSkip = 0 # skip past this number of frames to first frame of interest
        next_frame_skip = 1 # skip this many between successive frames
        nbNoms = 100
        nbRead = 2400
        
        # define mask
        y1,y2 = (50,420)
        x1,x2 = (100,490)


    # create mask
    mask[0:y1,:] = 0
    mask[y2:,:] = 0
    mask[:,0:x1] = 0
    mask[:,x2:] = 0
    return [file_desc,inFile,nbSkipNom,nbSkip,next_frame_skip,nbNoms,nbRead,mask]               