import numpy as np
from skimage.transform import warp, AffineTransform, resize
from utils import read_grave as rg
from utils import neighbors
from utils import custom_plots


dr,dc = (0.25, 0.25)
imgH, imgW = (512,640)
r, c = (int(imgH/2), int(imgW/2))

# Pyxis LWIR
# ___________
# | 45 | 0  |
# |---------|
# |135 | 90 |
# ----------- 

# define the locations of the four differently polarized sensors
p45 = [[i+j*imgW for i in range(imgW) if i%2==0]for j in range(imgH) if j%2==0]
p45idx = np.array([item for sublist in p45 for item in sublist]) # combine lists

p0 = [[i+1+j*imgW for i in range(imgW) if i%2==0]for j in range(imgH) if j%2==0]
p0idx = np.array([item for sublist in p0 for item in sublist]) # combine lists

p135 = [[i+j*imgW for i in range(imgW) if i%2==0]for j in range(imgH) if j%2==1]
p135idx = np.array([item for sublist in p135 for item in sublist]) # combine lists

p90 = [[i+1+j*imgW for i in range(imgW) if i%2==0]for j in range(imgH) if j%2==1]
p90idx = np.array([item for sublist in p90 for item in sublist]) # combine lists


'''

'''
def get_corrections(lowSeqFile, highSeqFile):

    # Read the LOW sequence file ******************************************************************
    numb_first_frame = 1
    numb_frames = -1 # (a positive integer, or -1 to read all)
    fileHdr, frameHdrList, metaHdrList, imgDataList = rg.read_grave(lowSeqFile,
                                                                    startFrame=numb_first_frame,
                                                                    nFramesToRead=numb_frames,
                                                                    verbose=False)

    # find out the data type (uint8, uint16, etc)
    imgs = np.array(imgDataList)
    img0 = imgs[0]
    shape = np.shape(img0) # examples: (512, 640) for a grayscale image, (512, 640, 3) for an RGB image
    depth = shape[-1] if len(shape)>2 else 1 # (or number of color channels)
    fdtype = img0.dtype
    #print(shape, depth, fdtype)

    lo_45_list  = []
    lo_0_list   = []
    lo_135_list = []
    lo_90_list  = []

    for ii in range(len(imgDataList)):
        img = imgDataList[ii]

        # flatten image
        fdata = img.reshape(imgH * imgW)

        I_45  = fdata[p45idx].reshape([r,c])  # s45 polarized sub-image
        I_0   = fdata[p0idx].reshape([r,c])   # s0 polarized sub-image
        I_135 = fdata[p135idx].reshape([r,c]) # s135 polarized sub-image
        I_90  = fdata[p90idx].reshape([r,c])  # s90 polarized sub-image

        lo_45_list.append(I_45)        
        lo_0_list.append(I_0)
        lo_135_list.append(I_135)
        lo_90_list.append(I_90)


    # Read the HIGH sequence file ******************************************************************
    numb_first_frame = 1
    numb_frames = -1 # (a positive integer, or -1 to read all)
    fileHdr, frameHdrList, metaHdrList, imgDataList = rg.read_grave(highSeqFile,
                                                                    startFrame=numb_first_frame,
                                                                    nFramesToRead=numb_frames,
                                                                    verbose=False)

    # find out the data type (uint8, uint16, etc)
    imgs = np.array(imgDataList)
    img0 = imgs[0]
    shape = np.shape(img0) # examples: (512, 640) for a grayscale image, (512, 640, 3) for an RGB image
    depth = shape[-1] if len(shape)>2 else 1 # (or number of color channels)
    fdtype = img0.dtype
    #print(shape, depth, fdtype)

    hi_45_list  = []
    hi_0_list   = []
    hi_135_list = []
    hi_90_list  = []

    for ii in range(len(imgDataList)):
        img = imgDataList[ii]

        # flatten image
        fdata = img.reshape(imgH * imgW)

        I_45  = fdata[p45idx].reshape([r,c])  # s90 polarized sub-image
        I_0   = fdata[p0idx].reshape([r,c])   # s0 polarized sub-image
        I_135 = fdata[p135idx].reshape([r,c]) # s135 polarized sub-image
        I_90  = fdata[p90idx].reshape([r,c])  # s45 polarized sub-image

        hi_45_list.append(I_45)
        hi_0_list.append(I_0)
        hi_135_list.append(I_135)
        hi_90_list.append(I_90)



    # Check for Bad Pixels *************************************************************************

    # ngidx_45 = []
    # ngidx_0 = []
    # ngidx_135 = []
    # ngidx_90 = []


    # # OHMSETT 2016 BAD PIXELS
    # ngidx_45 = [(41,67),(42,148),(78,226),(108,162),(123,17),(151,109)]
    # ngidx_0 = [(41,67),(43,147),(151,108)]
    # ngidx_135 = [(41,67),(44,148),(107,162),(150,109)]
    # ngidx_90 = [(6,155),(22,246),(90,292),(42,147),(150,109),(219,124)]


    # DECK_POOL_2019 BAD PIXELS
    ngidx_45 = []
    ngidx_0 = [(186,240)]
    ngidx_135 = []
    ngidx_90 = [(139,169)]


    # Read the DATA frames and apply NUC ************************************************************
    '''
                <Hi> - <Lo>
    Imgcorr = --------------- x (Im - Lo) + <Lo>
                 Hi  -  Lo


    where <Hi> is the maximum Hi-image value (a scalar).
          <Lo> is the minimum Lo-image value (a scalar).
           Hi  is the mean of the Hi sequence (a single image)
           Lo  is the mean of the Lo sequence (a single image)
           Im  is the current image from the data sequence
    '''

    # Calculate lo and Lo

    # right-shift by 2 since camera data is 14-bit stored in 16-bit datatype
    imgStack = np.array(lo_45_list, np.uint16)
    #imgStack = np.left_shift(imgStack, 2)
    # get minimum of each pixel in the stack
    lo_45 = np.mean(np.amin(imgStack, axis=0))
    #custom_plots.show_img_return_input(lo_45, 'lo_45', cm='gray', ask=True)
    # get a mean low image
    Lo_45 = np.mean(imgStack, axis=0)
    #custom_plots.show_img_return_input(Lo_45, 'Lo_45', cm='gray', ask=True)

    # right-shift by 2 since camera data is 14-bit stored in 16-bit datatype
    imgStack = np.array(lo_0_list, np.uint16)
    #imgStack = np.left_shift(imgStack, 2)
    # get minimum of each pixel in the stack
    lo_0 = np.mean(np.amin(imgStack, axis=0))
    #print("lo_0: ", lo_0)
    # get a mean low image
    Lo_0 = np.mean(imgStack, axis=0)
    #custom_plots.show_img_return_input(Lo_0, 'Lo_0', cm='gray', ask=True)

    # right-shift by 2 since camera data is 14-bit stored in 16-bit datatype
    imgStack = np.array(lo_135_list, np.uint16)
    #imgStack = np.left_shift(imgStack, 2)
    # get minimum of each pixel in the stack
    lo_135 = np.mean(np.amin(imgStack, axis=0))
    #custom_plots.show_img_return_input(lo_135, 'lo_135', cm='gray', ask=True)
    # get a mean low image
    Lo_135 = np.mean(imgStack, axis=0)
    #custom_plots.show_img_return_input(Lo_135, 'Lo_135', cm='gray', ask=True)

    # right-shift by 2 since camera data is 14-bit stored in 16-bit datatype
    imgStack = np.array(lo_90_list, np.uint16)
    #imgStack = np.left_shift(imgStack, 2)
    # get minimum of each pixel in the stack
    lo_90 = np.mean(np.amin(imgStack, axis=0))
    #print("lo_90: ", lo_90)
    # get a mean low image
    Lo_90 = np.mean(imgStack, axis=0)
    #custom_plots.show_img_return_input(Lo_90, 'Lo_90', cm='gray', ask=True)


    # Calculate hi and Hi

    # right-shift by 2 since camera data is 14-bit stored in 16-bit datatype
    imgStack = np.array(hi_45_list, np.uint16)
    #imgStack = np.left_shift(imgStack, 2)
    # get maximum of each pixel in the stack
    hi_45 = np.mean(np.amax(imgStack, axis=0))
    #custom_plots.show_img_return_input(hi_45, 'hi_45', cm='gray', ask=True)
    # get a mean high image
    Hi_45 = np.mean(imgStack, axis=0)
    #custom_plots.show_img_return_input(Hi_45, 'Hi_45', cm='gray', ask=True)

    # right-shift by 2 since camera data is 14-bit stored in 16-bit datatype
    imgStack = np.array(hi_0_list, np.uint16)
    #imgStack = np.left_shift(imgStack, 2)
    # get maximum of each pixel in the stack
    hi_0 = np.mean(np.amax(imgStack, axis=0))
    #print("hi_0: ", hi_0)
    # get a mean high image
    Hi_0 = np.mean(imgStack, axis=0)
    #custom_plots.show_img_return_input(Hi_0, 'Hi_0', cm='gray', ask=True)

    # right-shift by 2 since camera data is 14-bit stored in 16-bit datatype
    imgStack = np.array(hi_135_list, np.uint16)
    #imgStack = np.left_shift(imgStack, 2)
    # get maximum of each pixel in the stack
    hi_135 = np.mean(np.amax(imgStack, axis=0))
    #custom_plots.show_img_return_input(hi_135, 'hi_135', cm='gray', ask=True)
    # get a mean high image
    Hi_135 = np.mean(imgStack, axis=0)
    #custom_plots.show_img_return_input(Hi_135, 'Hi_135', cm='gray', ask=True)
    
    # right-shift by 2 since camera data is 14-bit stored in 16-bit datatype
    imgStack = np.array(hi_90_list, np.uint16)
    #imgStack = np.left_shift(imgStack, 2)
    # get maximum of each pixel in the stack
    hi_90 = np.mean(np.amax(imgStack, axis=0))
    #custom_plots.show_img_return_input(hi_90, 'hi_90', cm='gray', ask=True)
    # get a mean high image
    Hi_90 = np.mean(imgStack, axis=0)
    #custom_plots.show_img_return_input(Hi_90, 'Hi_90', cm='gray', ask=True)    

    return ( [ngidx_45, ngidx_0, ngidx_135, ngidx_90],
             [lo_45, lo_0, lo_135, lo_90, Lo_45, Lo_0, Lo_135, Lo_90], 
             [hi_45, hi_0, hi_135, hi_90, Hi_45, Hi_0, Hi_135, Hi_90] )





'''

'''
def get_images(ngidxs, LOs, HIs, img, upsize=True, products='stokes'):
    
    [ngidx_45, ngidx_0, ngidx_135, ngidx_90] = ngidxs
    [lo_45, lo_0, lo_135, lo_90, Lo_45, Lo_0, Lo_135, Lo_90] = LOs
    [hi_45, hi_0, hi_135, hi_90, Hi_45, Hi_0, Hi_135, Hi_90] = HIs

    #print("max: ", np.max(img), "mean: ", np.mean(img))

    # right-shift by 2 since camera data is 14-bit stored in 16-bit datatype
    #img = np.left_shift(img, 2)
    #print("max: ", np.max(img), "mean: ", np.mean(img))

    # flatten image
    fdata = img.reshape(imgH * imgW)
    
    I_45  = fdata[p45idx].reshape([r,c]).astype(np.float32)  # s45 polarized sub-image
    I_0   = fdata[p0idx].reshape([r,c]).astype(np.float32)   # s0 polarized sub-image
    I_135 = fdata[p135idx].reshape([r,c]).astype(np.float32) # s135 polarized sub-image
    I_90  = fdata[p90idx].reshape([r,c]).astype(np.float32)  # s90 polarized sub-image
    #custom_plots.show_img_return_input(I_90, 'I_90', cm='gray', ask=True) 

    imgnumb = 'x'

    # 45 sub-image
    Icorr_45 = ((hi_45-lo_45)/(Hi_45-Lo_45)) * (I_45 - Lo_45) + lo_45
    # Replace bad pixels
    if ngidx_45:
        for jj in range(len(ngidx_45)):
            replVal = neighbors.getReplacementValue(ngidx_45, jj, Icorr_45, 1)
            if replVal:
                Icorr_45[ngidx_45[jj][0], ngidx_45[jj][1]] = replVal

    # spatial alignment of the sub-image
    tform =  AffineTransform(translation=(dr, dc))
    Icorr_45 = warp(Icorr_45, tform.inverse).astype(np.uint16)
    Icorr_45[:,-1] = Icorr_45[:,-2]
    Icorr_45[0,:] = Icorr_45[1,:]

    if upsize:
        Icorr_45 = resize(Icorr_45, (imgH, imgW), preserve_range=True)
    #custom_plots.show_img_return_input(Icorr_45, '45 - '+imgnumb, cm='gray', ask=True)  


    # 0 sub-image
    Icorr_0 = ((hi_0-lo_0)/(Hi_0-Lo_0)) * (I_0 - Lo_0) + lo_0
    # Replace bad pixels
    if ngidx_0:
        for jj in range(len(ngidx_0)):
            replVal = neighbors.getReplacementValue(ngidx_0, jj, Icorr_0, 1)
            if replVal:
                Icorr_0[ngidx_0[jj][0], ngidx_0[jj][1]] = replVal

    # spatial alignment of the sub-image
    tform =  AffineTransform(translation=(dr, -dc))
    Icorr_0 = warp(Icorr_0, tform.inverse).astype(np.uint16)
    # the translation leaves a row and a column of zeros; replace with nearest rows/col
    Icorr_0[:,0] = Icorr_0[:,1]
    Icorr_0[-1,:] = Icorr_0[-2,:]

    if upsize:
        Icorr_0 = resize(Icorr_0, (imgH, imgW), preserve_range=True)
    #custom_plots.show_img_return_input(Icorr_0, '0 - '+imgnumb, cm='gray', ask=True)



    # 135 sub-image
    Icorr_135 = ((hi_135-lo_135)/(Hi_135-Lo_135)) * (I_135 - Lo_135) + lo_135
    # Replace bad pixels
    if ngidx_135:
        for jj in range(len(ngidx_135)):
            replVal = neighbors.getReplacementValue(ngidx_135, jj, Icorr_135, 1)
            if replVal:
                Icorr_135[ngidx_135[jj][0], ngidx_135[jj][1]] = replVal

    # spatial alignment of the sub-image
    tform =  AffineTransform(translation=(-dr, -dc))
    Icorr_135 = warp(Icorr_135, tform.inverse).astype(np.uint16)
    Icorr_135[:,-1] = Icorr_135[:,-2]
    Icorr_135[-1,:] = Icorr_135[-2,:]

    if upsize:
        Icorr_135 = resize(Icorr_135, (imgH, imgW), preserve_range=True)
    #custom_plots.show_img_return_input(Icorr_135, '135 - '+imgnumb, cm='gray', ask=True)


    # 90 sub-image
    Icorr_90 = ((hi_90-lo_90)/(Hi_90-Lo_90)) * (I_90 - Lo_90) + lo_90
    # Replace bad pixels
    if ngidx_90:
        for jj in range(len(ngidx_90)):
            replVal = neighbors.getReplacementValue(ngidx_90, jj, Icorr_90, 1)
            if replVal:
                Icorr_90[ngidx_90[jj][0], ngidx_90[jj][1]] = replVal

    # spatial alignment of the sub-image
    tform =  AffineTransform(translation=(-dr, dc))
    Icorr_90 = warp(Icorr_90, tform.inverse).astype(np.uint16)
    # the translation leaves a row and a column of zeros; replace with nearest rows/col
    Icorr_90[:,0] = Icorr_90[:,1]
    Icorr_90[0,:] = Icorr_90[1,:]

    if upsize:
        Icorr_90 = resize(Icorr_90, (imgH, imgW), preserve_range=True)
    
    #custom_plots.show_img_return_input(Icorr_90, '90 - '+imgnumb, cm='gray', ask=True)
      


    S0 = (Icorr_0 + Icorr_90)/2
    S1 = Icorr_0 - Icorr_90
    S2 = Icorr_45 - Icorr_135
    DoLP = np.sqrt(S1**2 + S2**2)/S0
    #AoLP = (np.arctan2(S2,S1)/2)*(180/np.pi)
    
    if products == 'stokes':
        return (S0, S1, S2, DoLP)
    elif products == 'pol':
        return (Icorr_45, Icorr_0, Icorr_135, Icorr_90)
    elif products == 'all':
        return(Icorr_45, Icorr_0, Icorr_135, Icorr_90, S0, S1, S2, DoLP)