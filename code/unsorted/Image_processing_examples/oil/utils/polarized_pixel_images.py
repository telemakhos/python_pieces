import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
import pickle
import numpy as np
from skimage.draw import polygon
from skimage.transform import warp, AffineTransform, resize
import h5py
from utils import neighbors

root_dir = "./DATA"

class Corrections():
    
    
    def __init__(self, sensor_name):
        
        if sensor_name.lower() == 'dipole':
            nuc_file = root_dir + "/" + "Corrections" + "/" + sensor_name + "/NUC.h5" # Gains, Offsets, BPRs
            replacement_candidates_file = root_dir + "/" + "Corrections" + "/" + sensor_name + "/bads_candidates.pkl"
            
            imgH, imgW = (720, 2560)
            r, c = (int(imgH/2), int(imgW/4)) # height and width of the four sub-images    
            self.submask = np.zeros((r,c), np.uint8)
            poly_vertices = np.array( ( (0,0),(0,c),(r,c),(r,200),(200,0) ) ) # exclude the bad lower-lefthand corner
            #poly_vertices = np.array( ( (0,0),(0,c),(r,c),(r,0) ) ) # include the entire sub-image
            rr, cc = polygon(poly_vertices[:,0], poly_vertices[:,1], (r,c))
            self.submask[rr,cc] = 1            
            
            # y,x values for affine transform
            # the DIPOLE polarized pixel pattern
            # ___________
            # | 135 | 0  |
            # |----------|
            # |  90 | 45 |
            # ----------- 
            dr,dc = (0.25, 0.25)
            # translation from center of pixel pattern
            self.xl135 = (-dr, -dc)
            self.xl0   = (-dr, dc)
            self.xl90  = (dr, -dc)
            self.xl45  = (dr, dc)            
            

            ''' Read the saved Gains, Offsets, & BadPixMaps from HDF5 file '''
            hf = h5py.File(nuc_file, 'r')

            self.Gain_mw_135 = hf['Gain_mw_135'].value
            self.Gain_mw_0 = hf['Gain_mw_0'].value
            self.Gain_mw_90 = hf['Gain_mw_90'].value
            self.Gain_mw_45 = hf['Gain_mw_45'].value

            self.Gain_lw_135 = hf['Gain_lw_135'].value
            self.Gain_lw_0 = hf['Gain_lw_0'].value
            self.Gain_lw_90 = hf['Gain_lw_90'].value
            self.Gain_lw_45 = hf['Gain_lw_45'].value

            self.Offset_mw_135 = hf['Offset_mw_135'].value
            self.Offset_mw_0 = hf['Offset_mw_0'].value
            self.Offset_mw_90 = hf['Offset_mw_90'].value
            self.Offset_mw_45 = hf['Offset_mw_45'].value

            self.Offset_lw_135 = hf['Offset_lw_135'].value
            self.Offset_lw_0 = hf['Offset_lw_0'].value
            self.Offset_lw_90 = hf['Offset_lw_90'].value
            self.Offset_lw_45 = hf['Offset_lw_45'].value    

            self.BadPix_mw_135 = hf['BadPix_mw_135'].value
            self.BadPix_mw_0 = hf['BadPix_mw_0'].value
            self.BadPix_mw_90 = hf['BadPix_mw_90'].value
            self.BadPix_mw_45 = hf['BadPix_mw_45'].value

            self.BadPix_lw_135 = hf['BadPix_lw_135'].value
            self.BadPix_lw_0 = hf['BadPix_lw_0'].value
            self.BadPix_lw_90 = hf['BadPix_lw_90'].value
            self.BadPix_lw_45 = hf['BadPix_lw_45'].value
            hf.close()


            ''' Read bad-pixel information from pickle '''
            with open(replacement_candidates_file, 'rb') as pf:
                bads_cands = pickle.load(pf)
                
            [bads_mw_0, bads_mw_45, bads_mw_90, bads_mw_135,
             bads_lw_0, bads_lw_45, bads_lw_90, bads_lw_135,
             candidates_mw_0, candidates_mw_45, candidates_mw_90, candidates_mw_135,
             candidates_lw_0, candidates_lw_45, candidates_lw_90, candidates_lw_135] = bads_cands

            self.bads_mw_135, self.candidates_mw_135 = neighbors.getReplacementIndexes(self.BadPix_mw_135, 2)
            self.bads_mw_0, self.candidates_mw_0 = neighbors.getReplacementIndexes(self.BadPix_mw_0, 2)
            self.bads_mw_90, self.candidates_mw_90 = neighbors.getReplacementIndexes(self.BadPix_mw_90, 2)
            self.bads_mw_45, self.candidates_mw_45 = neighbors.getReplacementIndexes(self.BadPix_mw_45, 2)

            self.bads_lw_135, self.candidates_lw_135 = neighbors.getReplacementIndexes(self.BadPix_lw_135, 2)
            self.bads_lw_0, self.candidates_lw_0 = neighbors.getReplacementIndexes(self.BadPix_lw_0, 2)
            self.bads_lw_90, self.candidates_lw_90 = neighbors.getReplacementIndexes(self.BadPix_lw_90, 2)
            self.bads_lw_45, self.candidates_lw_45 = neighbors.getReplacementIndexes(self.BadPix_lw_45, 2)


        elif sensor_name.lower() == 'pyxis_lwir':
            
            nuc_file = root_dir + "/" + "Corrections" + "/" + sensor_name + "/NUC.h5" # Gains, Offsets, BPRs
            replacement_candidates_file = root_dir + "/" + "Corrections" + "/" + sensor_name + "/bads_candidates.pkl"
            
            #print(nuc_file)
            #print(replacement_candidates_file)
            
            imgH, imgW = (512, 640)
            r, c = (int(imgH/2), int(imgW/2)) # height and width of the four sub-images    
            self.submask = np.zeros((r,c), np.uint8)
            poly_vertices = np.array( ( (0,0),(0,c),(r,c),(r,200),(200,0) ) ) # exclude the bad lower-lefthand corner
            #poly_vertices = np.array( ( (0,0),(0,c),(r,c),(r,0) ) ) # include the entire sub-image
            rr, cc = polygon(poly_vertices[:,0], poly_vertices[:,1], (r,c))
            self.submask[rr,cc] = 1            
            
            # y,x values for affine transform
            # the PYXIS LWIR polarized pixel pattern
            # ___________
            # |  45 | 0  |
            # |----------|
            # | 135 | 90 |
            # ----------- 
            dr,dc = (0.25, 0.25)
            # translation from center of pixel pattern
            self.xl45  = (-dr, -dc)
            self.xl0   = (-dr, dc)
            self.xl135 = (dr, -dc)
            self.xl90  = (dr, dc)       
            

            ''' Read the saved Gains, Offsets, & BadPixMaps from HDF5 file '''
            hf = h5py.File(nuc_file, 'r')

            self.Gain_135 = hf['Gain_lw_135'].value
            self.Gain_0 = hf['Gain_lw_0'].value
            self.Gain_90 = hf['Gain_lw_90'].value
            self.Gain_45 = hf['Gain_lw_45'].value

            self.Offset_135 = hf['Offset_lw_135'].value
            self.Offset_0 = hf['Offset_lw_0'].value
            self.Offset_90 = hf['Offset_lw_90'].value
            self.Offset_45 = hf['Offset_lw_45'].value  

            self.BadPix_135 = hf['BadPix_lw_135'].value
            self.BadPix_0 = hf['BadPix_lw_0'].value
            self.BadPix_90 = hf['BadPix_lw_90'].value
            self.BadPix_45 = hf['BadPix_lw_45'].value

            hf.close()


            ''' Read bad-pixel information from pickle '''
            with open(replacement_candidates_file, 'rb') as pf:
                bads_cands = pickle.load(pf)
                
            bads_0, bads_45, bads_90, bads_135, candidates_0, candidates_45, candidates_90, candidates_135 = bads_cands

            self.bads_135, self.candidates_135 = neighbors.getReplacementIndexes(self.BadPix_135, 2)
            self.bads_0, self.candidates_0 = neighbors.getReplacementIndexes(self.BadPix_0, 2)
            self.bads_90, self.candidates_90 = neighbors.getReplacementIndexes(self.BadPix_90, 2)
            self.bads_45, self.candidates_45 = neighbors.getReplacementIndexes(self.BadPix_45, 2)          
            
            
        else:
            print("Sensor array name not recognized.")



def get_sub_imgs(Img, sensor_name):
    
    imgH, imgW = Img.shape # height and width of the input image
    r, c = (int(imgH/2), int(imgW/2)) # height and width of the four sub-images
    
    if sensor_name.lower() == 'dipole':
        
        # the DIPOLE polarized pixel pattern
        # ___________
        # | 135 | 0  |
        # |----------|
        # |  90 | 45 |
        # ----------- 

        # define the locations of the four differently polarized sensors
        p135 = [[i+j*imgW for i in range(imgW) if i%2==0]for j in range(imgH) if j%2==0]
        p135idx = np.array([item for sublist in p135 for item in sublist]) # combine lists

        p0 = [[i+1+j*imgW for i in range(imgW) if i%2==0]for j in range(imgH) if j%2==0]
        p0idx = np.array([item for sublist in p0 for item in sublist]) # combine lists

        p90 = [[i+j*imgW for i in range(imgW) if i%2==0]for j in range(imgH) if j%2==1]
        p90idx = np.array([item for sublist in p90 for item in sublist]) # combine lists

        p45 = [[i+1+j*imgW for i in range(imgW) if i%2==0]for j in range(imgH) if j%2==1]
        p45idx = np.array([item for sublist in p45 for item in sublist]) # combine lists
        
        # # verify correct designation of the pixel polarizations
        # print("135:", p135idx[0:10])
        # print("  0:", p0idx[0:10])
        # print(" 90:", p90idx[0:10])
        # print(" 45:", p45idx[0:10])
        # # 135: [ 0  2  4  6  8 10 12 14 16 18]
        # #   0: [ 1  3  5  7  9 11 13 15 17 19]
        # #  90: [1280 1282 1284 1286 1288 1290 1292 1294 1296 1298]
        # #  45: [1281 1283 1285 1287 1289 1291 1293 1295 1297 1299]
        
        # flatten image
        fdata = Img.reshape(imgH * imgW)

        I_45  = fdata[p45idx].reshape([r,c])  # s45 polarized sub-image
        I_0   = fdata[p0idx].reshape([r,c])   # s0 polarized sub-image
        I_135 = fdata[p135idx].reshape([r,c]) # s135 polarized sub-image
        I_90  = fdata[p90idx].reshape([r,c])  # s90 polarized sub-image

        
    elif sensor_name.lower() == 'pyxis_lwir':
        
        imgH, imgW = Img.shape # height and width of the input image
        r, c = (int(imgH/2), int(imgW/2))
        
        # the PYXIS LWIR polarized pixel pattern
        # ___________
        # |  45 | 0  |
        # |----------|
        # | 135 | 90 |
        # ----------- 

        # define the locations of the four differently polarized sensors
        p45 = [[i+j*imgW for i in range(imgW) if i%2==0]for j in range(imgH) if j%2==0]
        p45idx = np.array([item for sublist in p45 for item in sublist]) # combine lists

        p0 = [[i+1+j*imgW for i in range(imgW) if i%2==0]for j in range(imgH) if j%2==0]
        p0idx = np.array([item for sublist in p0 for item in sublist]) # combine lists

        p135 = [[i+j*imgW for i in range(imgW) if i%2==0]for j in range(imgH) if j%2==1]
        p135idx = np.array([item for sublist in p135 for item in sublist]) # combine lists

        p90 = [[i+1+j*imgW for i in range(imgW) if i%2==0]for j in range(imgH) if j%2==1]
        p90idx = np.array([item for sublist in p90 for item in sublist]) # combine lists


        # # verify correct designation of the pixel polarizations
        # print(" 45:", p45idx[0:10])
        # print("  0:", p0idx[0:10])
        # print("135:", p135idx[0:10])
        # print(" 90:", p90idx[0:10])
        # #  45: [ 0  2  4  6  8 10 12 14 16 18]
        # #   0: [ 1  3  5  7  9 11 13 15 17 19]
        # # 135: [320 322 324 326 328 330 332 334 336 338]
        # #  90: [321 323 325 327 329 331 333 335 337 339]
        
        # flatten image
        fdata = Img.reshape(imgH * imgW)

        I_45  = fdata[p45idx].reshape([r,c])  # s45 polarized sub-image
        I_0   = fdata[p0idx].reshape([r,c])   # s0 polarized sub-image
        I_135 = fdata[p135idx].reshape([r,c]) # s135 polarized sub-image
        I_90  = fdata[p90idx].reshape([r,c])  # s90 polarized sub-image    
        

    else:
        print("Sensor array name not recognized.")
        
    return (I_0, I_45, I_90, I_135)
        
    
    
    
    
def get_corr_sub_imgs(I, sensor_name, corr, upsize=True):
    
    imgH, imgW = I.shape
    
    if sensor_name.lower() == 'dipole':
    
        #split the image into MWIR and LWIR halves
        Imw = I[:,0:imgW//2]
        Ilw = I[:,imgW//2:imgW]
        
        # get the 4 sub-images for both wavebands
        I_MW_P0, I_MW_P45, I_MW_P90, I_MW_P135 = get_sub_imgs(Imw, sensor_name)
        I_LW_P0, I_LW_P45, I_LW_P90, I_LW_P135 = get_sub_imgs(Ilw, sensor_name)
        
        #apply the NUC, then replace the bad pixels
        '''(1/8) MWIR - 0 deg. '''
        #custom_plots.show_img(I_MW_P0, 'I_MW_P0', cm='gray', ask=True)
        Inuc = (I_MW_P0 * corr.Gain_mw_0) + corr.Offset_mw_0
        Inuc[Inuc<0] = 0
        Inuc[Inuc>65535] = 65535
        Inuc = neighbors.replaceBadPixels(Inuc, corr.bads_mw_0, corr.candidates_mw_0, corr.submask)
        tform = AffineTransform(translation=corr.xl0)
        I_MW_P0_corr = warp(Inuc, tform.inverse)
        #custom_plots.show_img(I_MW_P0_corr, 'I_MW_P0_corr', cm='gray', ask=True)
        
        '''(2/8) MWIR - 45 deg. '''
        #custom_plots.show_img(I_MW_P45, 'I_MW_P45', cm='gray', ask=True)
        Inuc = (I_MW_P45 * corr.Gain_mw_45) + corr.Offset_mw_45
        Inuc[Inuc<0] = 0
        Inuc[Inuc>65535] = 65535
        Inuc = neighbors.replaceBadPixels(Inuc, corr.bads_mw_45, corr.candidates_mw_45, corr.submask)
        tform = AffineTransform(translation=corr.xl45)
        I_MW_P45_corr = warp(Inuc, tform.inverse)    
        #custom_plots.show_img(I_MW_P45_corr, 'I_MW_P45_corr', cm='gray', ask=True)
        
        '''(3/8) MWIR - 90 deg. '''
        #custom_plots.show_img(I_MW_P90, 'I_MW_P90', cm='gray', ask=True)
        Inuc = (I_MW_P90 * corr.Gain_mw_90) + corr.Offset_mw_90
        Inuc[Inuc<0] = 0
        Inuc[Inuc>65535] = 65535
        Inuc = neighbors.replaceBadPixels(Inuc, corr.bads_mw_90, corr.candidates_mw_90, corr.submask)
        tform = AffineTransform(translation=corr.xl90)
        I_MW_P90_corr = warp(Inuc, tform.inverse)    
        #custom_plots.show_img(I_MW_P90_corr, 'I_MW_P90_corr', cm='gray', ask=True)
        
        '''(4/8) MWIR - 135 deg. '''
        #custom_plots.show_img(I_MW_P135, 'I_MW_P135', cm='gray', ask=True)
        Inuc = (I_MW_P135 * corr.Gain_mw_135) + corr.Offset_mw_135
        Inuc[Inuc<0] = 0
        Inuc[Inuc>65535] = 65535
        Inuc = neighbors.replaceBadPixels(Inuc, corr.bads_mw_135, corr.candidates_mw_135, corr.submask)
        tform = AffineTransform(translation=corr.xl135)
        I_MW_P135_corr = warp(Inuc, tform.inverse)    
        #custom_plots.show_img(I_MW_P135_corr, 'I_MW_P135_corr', cm='gray', ask=True)
        
        '''(5/8) LWIR - 0 deg. '''
        #custom_plots.show_img(I_LW_P0, 'I_LW_P0', cm='gray', ask=True)
        Inuc = (I_LW_P0 * corr.Gain_lw_0) + corr.Offset_lw_0
        Inuc[Inuc<0] = 0
        Inuc[Inuc>65535] = 65535
        Inuc = neighbors.replaceBadPixels(Inuc, corr.bads_lw_0, corr.candidates_lw_0, corr.submask)
        tform = AffineTransform(translation=corr.xl0)
        I_LW_P0_corr = warp(Inuc, tform.inverse)    
        #custom_plots.show_img(I_LW_P0_corr, 'I_LW_P0_corr', cm='gray', ask=True)
        
        '''(6/8) LWIR - 45 deg. '''
        #custom_plots.show_img(I_LW_P45, 'I_LW_P45', cm='gray', ask=True)
        Inuc = (I_LW_P45 * corr.Gain_lw_45) + corr.Offset_lw_45
        Inuc[Inuc<0] = 0
        Inuc[Inuc>65535] = 65535
        Inuc = neighbors.replaceBadPixels(Inuc, corr.bads_lw_45, corr.candidates_lw_45, corr.submask)
        tform = AffineTransform(translation=corr.xl45)
        I_LW_P45_corr = warp(Inuc, tform.inverse)
        #custom_plots.show_img(I_LW_P45_corr, 'I_LW_P45_corr', cm='gray', ask=True)
        
        '''(7/8) LWIR - 90 deg. '''
        #custom_plots.show_img(I_LW_P90, 'I_LW_P90', cm='gray', ask=True)
        Inuc = (I_LW_P90 * corr.Gain_lw_90) + corr.Offset_lw_90
        Inuc[Inuc<0] = 0
        Inuc[Inuc>65535] = 65535
        Inuc = neighbors.replaceBadPixels(Inuc, corr.bads_lw_90, corr.candidates_lw_90, corr.submask)
        tform = AffineTransform(translation=corr.xl90)
        I_LW_P90_corr = warp(Inuc, tform.inverse)    
        #custom_plots.show_img(I_LW_P90_corr, 'I_LW_P90_corr', cm='gray', ask=True)
        
        '''(8/8) LWIR - 135 deg. '''
        #custom_plots.show_img(I_LW_P135, 'I_LW_P135', cm='gray', ask=True)
        Inuc = (I_LW_P135 * corr.Gain_lw_135) + corr.Offset_lw_135
        Inuc[Inuc<0] = 0
        Inuc[Inuc>65535] = 65535
        Inuc = neighbors.replaceBadPixels(Inuc, corr.bads_lw_135, corr.candidates_lw_135, corr.submask)
        tform = AffineTransform(translation=corr.xl0)
        I_LW_P135_corr = warp(Inuc, tform.inverse)    
        #custom_plots.show_img(I_LW_P135_corr, 'I_LW_P135_corr', cm='gray', ask=True)
        
        if upsize:
            I_MW_P0_corr = resize(I_MW_P0_corr, (imgH, imgW//2))
            I_MW_P45_corr = resize(I_MW_P45_corr, (imgH, imgW//2))
            I_MW_P90_corr = resize(I_MW_P90_corr, (imgH, imgW//2))
            I_MW_P135_corr = resize(I_MW_P135_corr, (imgH, imgW//2))
            I_LW_P0_corr = resize(I_LW_P0_corr, (imgH, imgW//2))
            I_LW_P45_corr = resize(I_LW_P45_corr, (imgH, imgW//2))
            I_LW_P90_corr = resize(I_LW_P90_corr, (imgH, imgW//2))
            I_LW_P135_corr = resize(I_LW_P135_corr, (imgH, imgW//2))            
        
        
        return(I_MW_P0_corr, I_MW_P45_corr, I_MW_P90_corr, I_MW_P135_corr, I_LW_P0_corr, I_LW_P45_corr, I_LW_P90_corr, I_LW_P135_corr)
        
        
    elif sensor_name.lower() == 'pyxis_lwir':
        
        # get the 4 sub-images for both wavebands
        I_P0, I_P45, I_P90, I_P135 = get_sub_imgs(I, sensor_name)
        
        #apply the NUC, then replace the bad pixels
        '''(1/4) 0 deg. '''
        #custom_plots.show_img(I_MW_P0, 'I_MW_P0', cm='gray', ask=True)
        Inuc = (I_P0 * corr.Gain_0) + corr.Offset_0
        Inuc[Inuc<0] = 0
        Inuc[Inuc>65535] = 65535
        Inuc = neighbors.replaceBadPixels(Inuc, corr.bads_0, corr.candidates_0, corr.submask)
        tform = AffineTransform(translation=corr.xl0)
        I_P0_corr = warp(Inuc, tform.inverse)
        #custom_plots.show_img(I_P0_corr, 'I_P0_corr', cm='gray', ask=True)
        
        '''(2/4) 45 deg. '''
        #custom_plots.show_img(I_P45, 'I_P45', cm='gray', ask=True)
        Inuc = (I_P45 * corr.Gain_45) + corr.Offset_45
        Inuc[Inuc<0] = 0
        Inuc[Inuc>65535] = 65535
        Inuc = neighbors.replaceBadPixels(Inuc, corr.bads_45, corr.candidates_45, corr.submask)
        tform = AffineTransform(translation=corr.xl45)
        I_P45_corr = warp(Inuc, tform.inverse)    
        #custom_plots.show_img(I_P45_corr, 'I_P45_corr', cm='gray', ask=True)
        
        '''(3/4) 90 deg. '''
        #custom_plots.show_img(I_P90, 'I_P90', cm='gray', ask=True)
        Inuc = (I_P90 * corr.Gain_90) + corr.Offset_90
        Inuc[Inuc<0] = 0
        Inuc[Inuc>65535] = 65535
        Inuc = neighbors.replaceBadPixels(Inuc, corr.bads_90, corr.candidates_90, corr.submask)
        tform = AffineTransform(translation=corr.xl90)
        I_P90_corr = warp(Inuc, tform.inverse)    
        #custom_plots.show_img(I_P90_corr, 'I_P90_corr', cm='gray', ask=True)
        
        '''(4/4) 135 deg. '''
        #custom_plots.show_img(I_P135, 'I_P135', cm='gray', ask=True)
        Inuc = (I_P135 * corr.Gain_135) + corr.Offset_135
        Inuc[Inuc<0] = 0
        Inuc[Inuc>65535] = 65535
        Inuc = neighbors.replaceBadPixels(Inuc, corr.bads_135, corr.candidates_135, corr.submask)
        tform = AffineTransform(translation=corr.xl135)
        I_P135_corr = warp(Inuc, tform.inverse)    
        #custom_plots.show_img(I_P135_corr, 'I_P135_corr', cm='gray', ask=True)
        
        if upsize:
            I_P0_corr = resize(I_P0_corr, (imgH, imgW))
            I_P45_corr = resize(I_P45_corr, (imgH, imgW))
            I_P90_corr = resize(I_P90_corr, (imgH, imgW))
            I_P135_corr = resize(I_P135_corr, (imgH, imgW))
        
        return(I_P0_corr, I_P45_corr, I_P90_corr, I_P135_corr)
        
    else:
        print("Sensor array name not recognized.")
        print("Possible sensor names are \"dipole\", \"pyxis_lwir\"")        