import numpy as np
from skimage import exposure
from skimage.restoration import denoise_bilateral
from skimage.morphology import disk
from skimage.filters import median, unsharp_mask, gaussian, difference_of_gaussians
from skimage.restoration import denoise_nl_means, estimate_sigma
from skimage.restoration import calibrate_denoiser, denoise_wavelet
from skimage.metrics import peak_signal_noise_ratio
#from medpy.filter import smoothing
from functools import partial


def get_img_min_mean_max(img):
    found_a_NaN = np.isnan(np.sum(img))
    return [np.nanmin(img), np.nanmean(img), np.nanmax(img), found_a_NaN]    

def print_img_info(img):
    v = get_img_min_mean_max(img)
    vmin =  f'{v[0]:.2f}'
    vmean = f'{v[1]:.2f}'
    vmax =  f'{v[2]:.2f}'
    dtype = img.dtype
    print(" :: ".join([vmin, vmean, vmax]), "  ", dtype)

def clip(in_image, lp = 0.5, up = 99.0):
    Img = in_image.copy()
    dtype = Img.dtype
    vmin = np.percentile(np.percentile(Img, lp, axis=0), lp).astype(dtype)
    vmax = np.percentile(np.percentile(Img, up, axis=0), up).astype(dtype)
    Img[Img<vmin] = vmin
    Img[Img>vmax] = vmax    
    return Img

def clip_low_zero(in_image, lp = 0.5, up = 99.0):
    Img = in_image.copy()
    imgH, imgW = Img.shape
    vmin, vmean, vmax, a_nan = get_img_min_mean_max(Img)
    if a_nan: # replace and NaNs with another value
        ix = np.isnan(Img)
        idx = np.where(ix)
        Img[idx] = vmin
    dtype = Img.dtype
    vmin = np.percentile(np.percentile(Img, lp, axis=0), lp).astype(dtype)
    vmax = np.percentile(np.percentile(Img, up, axis=0), up).astype(dtype)
    Img[Img<vmin] = 0
    Img[Img>vmax] = vmax    
    return Img

def clip_low_high_zero(in_image, lp = 0.5, up = 99.0):
    Img = in_image.copy()
    imgH, imgW = Img.shape
    vmin, vmean, vmax, a_nan = get_img_min_mean_max(Img)
    if a_nan: # replace and NaNs with another value
        ix = np.isnan(Img)
        idx = np.where(ix)
        Img[idx] = vmin
    dtype = Img.dtype
    vmin = np.percentile(np.percentile(Img, lp, axis=0), lp).astype(dtype)
    vmax = np.percentile(np.percentile(Img, up, axis=0), up).astype(dtype)
    Img[Img<vmin] = 0
    Img[Img>vmax] = 0   
    return Img

def denoise_etc(image):
    filtered_image = difference_of_gaussians(image, 1.5)
    return filtered_image


parameter_ranges = {'sigma': np.arange(0.1, 0.3, 0.02),
                    'wavelet': ['db1', 'db2'],
                    'convert2ycbcr': [True, False],
                    'multichannel': [True]}
def calibrated_denoiser(noisy):
    imgH, imgW = noisy.shape
    img = np.zeros((imgH, imgW, 3))
    img[:,:,0] = noisy
    img[:,:,1] = noisy
    img[:,:,2] = noisy
    _denoise_wavelet = partial(denoise_wavelet, rescale_sigma=True)
    calibrated_denoiser = calibrate_denoiser(img,
                                             _denoise_wavelet,
                                             denoise_parameters=parameter_ranges)
    # Denoised image using calibrated denoiser
    calibrated_output = calibrated_denoiser(img)
    return calibrated_output

def show_dets_on_image(img, detimg):
    imgH, imgW = img.shape
    vmin, vmean, vmax, a_nan = get_img_min_mean_max(img)
    if a_nan: # replace and NaNs with another value
        ix = np.isnan(img)
        idx = np.where(ix)
        img[idx] = vmin
    # rescale to range [0,1]
    if vmin < 0:
        img += -1*vmin
    img /= np.max(img)
    img *= 255
    Irgb = np.zeros((imgH,imgW,3), np.uint8)
    Irgb[:,:,0] = img
    Irgb[:,:,1] = img
    Irgb[:,:,2] = img
    Irgb[:,:,0][detimg>0] = 255
    Irgb[:,:,1][detimg>0] = 0
    Irgb[:,:,2][detimg>0] = 0
    return Irgb

def rescale_from_0_to_1(img):
    vmin, vmean, vmax, a_nan = get_img_min_mean_max(img)
    if a_nan: # replace and NaNs with another value
        ix = np.isnan(img)
        idx = np.where(ix)
        img[idx] = vmin
    # must rescale to range [0,1] to use equalize_adapthist()
    if vmin < 0:
        img += -1*vmin
    img /= np.nanmax(img)
    return img

def rescale_from_1_to_xmax(img, xmax):
    vmin, vmean, vmax, a_nan = get_img_min_mean_max(img)
    if a_nan: # replace and NaNs with another value
        ix = np.isnan(img)
        idx = np.where(ix)
        img[idx] = vmin
    # must rescale to range [0,1] to use equalize_adapthist()
    if vmin < 1:
        img += -1*vmin
    if vmin > 1:
        img += 1-vmin
    img /= np.nanmax(img)
    img *= xmax
    return img

def denoise_blf(noisy):
    imgH, imgW = noisy.shape
    vmin, vmean, vmax, a_nan = get_img_min_mean_max(noisy)
    if a_nan: # replace and NaNs with another value
        ix = np.isnan(noisy)
        idx = np.where(ix)
        noisy[idx] = vmin
    denoised = denoise_bilateral(noisy, sigma_color=0.02, sigma_spatial=10)
    return denoised
   
def denoise_ad(noisy):
    imgH, imgW = noisy.shape
    vmin, vmean, vmax, a_nan = get_img_min_mean_max(noisy)
    if a_nan: # replace and NaNs with another value
        ix = np.isnan(noisy)
        idx = np.where(ix)
        noisy[idx] = vmin
    denoised = smoothing.anisotropic_diffusion(noisy, niter=5, kappa=50, gamma=0.3)
    return denoised
   
def median_filter(img):
    med = median(img, disk(3))
    return med

def unsharp(img):
    umimg = unsharp_mask(img, radius=3, amount=2)
    return umimg

def avg_bkg_sub(img):
    img -= np.mean(img)
    img[img<0] = 0
    return img

def denoise_nl(noisy):
    sigma_est = np.mean(estimate_sigma(noisy, multichannel=True))
    patch_kw = dict(patch_size=3,      # 3x3 patches
                    patch_distance=3,  # 7x7 search area
                    multichannel=True)
    denoise_fast = denoise_nl_means(noisy, h=0.8 * sigma_est, fast_mode=True, **patch_kw)
    return denoise_fast

def enhance_contrast(img):
    vmin, vmean, vmax, a_nan = get_img_min_mean_max(img)
    if a_nan: # replace and NaNs with another value
        ix = np.isnan(img)
        idx = np.where(ix)
        img[idx] = vmin
    # must rescale to range [0,1] to use equalize_adapthist()
    if vmin < 0:
        img += -1*vmin
    img /= np.nanmax(img)
    # adaptive histogram equalization
    img = exposure.equalize_adapthist(img, clip_limit=0.03)
    ## or use clipping
    #img = clip_upper_lower.clip(img, lp=0.10, up=99.0)
    return img

def invert_image(img):
    vmin, vmean, vmax, a_nan = get_img_min_mean_max(img)
    if a_nan: # replace and NaNs with another value
        ix = np.isnan(img)
        idx = np.where(ix)
        img[idx] = vmin
    #max_possible_value = np.finfo(img.dtype).max
    img = vmax - img
    return img
    
def get_positive_shifted(img):
    vmin, vmean, vmax, a_nan = get_img_min_mean_max(img)
    if a_nan: # replace and NaNs with another value
        ix = np.isnan(img)
        idx = np.where(ix)
        img[idx] = 0
    # must rescale to range [0,1] to use equalize_adapthist()
    if vmin < 0:
        img += np.abs(vmin)
    img /= np.nanmax(img)
    # adaptive histogram equalization
    img = exposure.equalize_adapthist(img, clip_limit=0.05)
    return img

def get_neg_pos_images(img, mask):
    vmin, vmean, vmax, a_nan = get_img_min_mean_max(img)
    if a_nan: # replace and NaNs with another value
        ix = np.isnan(img)
        idx = np.where(ix)
        img[idx] = vmin
    # must rescale to range [0,1] to use equalize_adapthist()
    s1pos = np.zeros_like(img)
    s1neg = np.zeros_like(img)
    if vmin < 0:
        idx = np.where(img<0)
        s1neg[idx] = img[idx]
    idx = np.where(img>0)
    s1pos[idx] = img[idx]
    s1pos *= mask
    s1neg *= -1
    s1neg[s1neg<0] = 0
    s1neg *= mask
    return s1neg, s1pos
    
def get_negative_s1(img):
    vmin, vmean, vmax, a_nan = get_img_min_mean_max(img)
    if a_nan: # replace and NaNs with another value
        ix = np.isnan(img)
        idx = np.where(ix)
        img[idx] = vmin
    # must rescale to range [0,1] to use equalize_adapthist()
    img[img>0] = 0.0
    img += -1*vmin
    img /= np.max(img)
    # adaptive histogram equalization
    img = exposure.equalize_adapthist(img, clip_limit=0.03)
    return img

def convert_img_to_uint8(Img):
    minVal = np.min(Img)
    Img -= minVal
    maxVal = np.max(Img)
    Img /= maxVal
    Img *= 255
    Img = Img.astype(np.uint8)
    return Img


