input file: Data/image_dir/2020-06-15_22-08-01_195/195.jpg

Local Time hour: 22
Local Time minute: 08
Local Time second: 01

***** Greenwich Time ****
day: 16
month: 6
year: 2020
hour: 3
minute: 8
second: 1

pitch: 0.259
roll: 4.754
longitude: -84.514
use center pixel: TRUE
use tilt correction: FALSE

number of stars: 15

Apparent Greenwich Sidereal Time: 311.853

***** StarChannel results *****
   right ascension (deg.): 227.339
       declination (deg.): 39.0925
    rotation angle (deg.): -163.334

***** Brightest Stars Identified *****
The star Seginus (γBoo) (1679.47, 860.67)
The star 33Boo (1673.07, 208.41)
The star 40Boo (1154.19, 637.04)
The star Nekkar (βBoo) (1140.12, 511.84)
The star δBoo (658.86, 1138.88)
The star 50Boo (515.24, 1134.46)
The star Alkalurops (μ1Boo) (611.85, 675.67)
The star ν1Boo (609.22, 292.49)
The star ν2Boo (595.77, 280.14)
The star μCrB (462.24, 440.49)
The star φBoo (463.64, 290.78)
The star ζ2CrB (293.63, 641.63)
The star πCrB (39.48, 1009.07)
The star κCrB (20.87, 642.20)
The star λCrB (28.28, 382.50)

***** Constellations Identified *****
Part of the constellation Bootes (Boo)

solved in 734 microseconds.
