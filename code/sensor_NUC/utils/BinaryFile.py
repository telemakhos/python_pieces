import sys
from os.path import getsize
import struct
import numpy as np


class Reader():
    def __init__(self, filepath, startFrame, nextFrameSkip, nbRead):
        self.filepath = filepath
        self.startFrame = startFrame # start reading at this frame number
        self.nextFrameSkip = nextFrameSkip # skip this many between successive frames
        self.nbRead = nbRead # total frames to read
        self.file_header_size = 2048
        self.imgDataType = None
        self.imgH = None
        self.imgW = None
        self.depth = None
        self.frhsize = None
        self.metasize = None 
        self.nbytes = None
        self.imageSize = None
        self.frameSize = None
        self.totalFrames = None
        self.foffset = 0 # the file offset in bytes
        self.f = None # the file pointer
        self.img = None # the image from the current frame
        self.frame_header = None
        self.meta_header = None
        self.isOpen = None
        self.nbytes_dict = { 'uint8':1, 'uint16':2, 'uint32':4,
                             'float32':4, 'float64':8, 'RGB24Bit':1 }
        self._get_file_info()
        self._open_file()
        self.frame_number = int(startFrame)
        self.next_frame_number = int(startFrame+nextFrameSkip+1)

    def get_next_image(self):
        if self.frhsize > 0:
            if self.f:
                frmHdr = np.memmap(self.f, dtype='uint8', mode='r', offset=self.foffset,
                                   shape=self.frhsize).tolist()
            else:
                print("Cannot increment file pointer; file %s is not open."
                      % (self.filepath.split('/')[-1]))
                sys.exit()
            self.frame_header = str(bytes(frmHdr), 'utf-8')
            # move file pointer past this frame's frame header
            self.foffset += self.frhsize
        if self.metasize >0:
            metaHdr = np.memmap(self.f, dtype='uint8', mode='r', offset=self.foffset,
                                shape=self.metasize).tolist()
            self.meta_header = str(bytes(metaHdr), 'utf-8')
            # move file pointer past this frame's meta header
            self.foffset += self.metasize 

        # read the frame's image data
        if self.imgDataType == 'RGB24Bit':
            frameData = np.memmap(self.f, dtype='uint8', mode='r', offset=self.foffset,
                                  shape=(self.imgH*self.imgW*3))
            temp0 = frameData[0::3].copy()
            temp1 = frameData[1::3].copy()
            temp2 = frameData[2::3].copy()
            temp0 = temp0.reshape(self.imgH, self.imgW)
            temp1 = temp1.reshape(self.imgH, self.imgW)
            temp2 = temp2.reshape(self.imgH, self.imgW)
            self.img = np.empty((self.imgH, self.imgW, 3), np.uint8)
            self.img[:,:,0] = temp0
            self.img[:,:,1] = temp1
            self.img[:,:,2] = temp2
        else:            
            frameData = np.memmap(self.f, dtype=self.imgDataType, mode='r', offset=self.foffset,
                                  shape=(self.imgH*self.imgW*self.depth))
            self.img = frameData.reshape(self.imgH, self.imgW, self.depth)
        # move past this frame's image data
        self.foffset += self.imageSize
        # check if the file should be closed
        next_frame_number = self.frame_number + self.nextFrameSkip + 1
        if (next_frame_number >= self.totalFrames) or (next_frame_number >= self.nbRead):
            self._close_file()
        # move past any skipped frames (headers + image data)
        self.foffset += (self.nextFrameSkip) * self.frameSize
        self.frame_number += (self.nextFrameSkip + 1)
        return self.img.copy()

    def _get_file_info(self):
        with open(self.filepath, 'rb'):
            self._getFileHeaderInfo(False)

    def _open_file(self):
        self.f = open(self.filepath, 'rb')
        self.isOpen = True
        if self.imgDataType == 'RGB24Bit':
            mult = 3
        else:
            mult = 1
        self.imageSize = self.imgH * self.imgW * self.depth * self.nbytes * mult
        self.frameSize = self.frhsize + self.metasize + self.imageSize
        self.foffset = self.file_header_size
        self.foffset += self.startFrame * self.frameSize

    def _close_file(self):
        self.f.close()
        self.isOpen = False

    def _getFileHeaderInfo(self, verbose=False):
        filesize = getsize(self.filepath)
        if verbose: print('filesize =', filesize)
        # Read in header, then rest of binary file
        with open(self.filepath, 'rb') as f:
            header = f.read(self.file_header_size)
            #print(header, '\n')
            hList = header.decode('utf-8').split('\n')
        # Read Data Type
        fnfmt = [x.split('FILE_NUMERICAL_FORMAT=')[-1].split('\r')[0]
                     for x in hList
                     if x.startswith('FILE_NUMERICAL_FORMAT=')]
        if len(fnfmt)>0:
            imgDataType = fnfmt[0]
        else: # if not readable, assume uint16
            imgDataType = 'uint16'

        if imgDataType in ['uint8', 'UINT8']:
            imgDataType = 'uint8'
        if imgDataType in ['FLOAT', 'float', 'float32', 'Float32']:
            imgDataType = 'float32'
        if imgDataType in ['FLOAT64', 'float64', 'Float64']:
            imgDataType = 'float64'
        # Read Image Height
        imgHeight = [x.split('IMAGE_HEIGHT=')[-1].split('\r')[0]
                     for x in hList
                     if x.startswith('IMAGE_HEIGHT=')]
        if len(imgHeight)>0:
            imgH = int(imgHeight[0])
        else:
            imgH = 0
        # Read Image Width
        imgWidth = [x.split('IMAGE_WIDTH=')[-1].split('\r')[0]
                     for x in hList
                     if x.startswith('IMAGE_WIDTH=')]
        if len(imgWidth)>0:
            imgW = int(imgWidth[0])
        else:
            imgW = 0
        # Read Image Depth
        imgDepth = [x.split('IMAGE_DEPTH=')[-1].split('\r')[0]
                     for x in hList
                     if x.startswith('IMAGE_DEPTH=')]
        if len(imgDepth)>0:
            depth = int(imgDepth[0])
        else:
            depth = 1 # defaults to 1 color layer
        # Read FRAME_HEADER_SIZE in bytes
        frhsize = [x.split('FRAME_HEADER_SIZE=')[-1].split('\r')[0]
                     for x in hList
                     if x.startswith('FRAME_HEADER_SIZE=')]
        if len(frhsize)>0:
            frhsize = int(frhsize[0])
        else:
            frhsize = 0
        # Read META_HEADER_SIZE in bytes
        metasize = [x.split('META_HEADER_SIZE=')[-1].split('\r')[0]
                     for x in hList
                     if x.startswith('META_HEADER_SIZE=')]
        if len(metasize)>0:
            metasize = int(metasize[0])
        else:
            metasize = 0
        nbytes = self.nbytes_dict[imgDataType]
        if verbose: print('filesize', 'imgW', 'imgH', 'depth', 'nbytes', 'frhsize', 'metasize')
        if verbose: print(filesize, imgW, imgH, depth, nbytes, frhsize, metasize)
        if imgDataType == 'RGB24Bit':
            totalFrames = int( (filesize - self.file_header_size)/((imgH*imgW*3*nbytes)+frhsize+metasize) )
        else:
            totalFrames = int( (filesize - self.file_header_size)/((imgH*imgW*depth*nbytes)+frhsize+metasize) )
        if verbose: print('total frames:', totalFrames)     
        self.imgDataType = imgDataType
        self.imgH = imgH
        self.imgW = imgW
        self.depth = depth
        self.frhsize = frhsize
        self.metasize = metasize
        self.nbytes = nbytes
        self.totalFrames = totalFrames
        if self.nbRead == -1: self.nbRead = self.totalFrames
        if self.nbRead > self.totalFrames: self.nbRead = self.totalFrames


########################################################################

class Writer():
    def __init__(self, filepath, options):
        self.filepath = filepath
        self.file_header_size = 2048
        self.frhsize = 26 if options['write_frame_headers'] else 0
        self.metasize = 26 if options['write_meta_headers'] else 0
        self.nbytes_dict = {'uint8':1, 'uint16':2, 'uint32':4, 'float32':4, 'float64':8}
        self.fmt_dict = {'uint8':'B', 'uint16':'H', 'uint32':'L', 'float32':'f', 'float64':'d'}
        self.limits_dict = { 'uint8':{'min':0, 'max':255},
                             'uint16':{'min':0, 'max':65535},
                             'uint32':{'min':0, 'max':4294967295},
                             'float32':{'min':-2147483648, 'max':2147483647},
                             'float64':{'min':-9223372036854775808, 'max':9223372036854775807}
                           }
        self.imgH = options['imgH']
        self.imgW = options['imgW']
        self.depth = options['imgD']
        self.dtype = options['dtype']
        self.fmt = str(self.imgH*self.imgW*self.depth)+self.fmt_dict[self.dtype]
        self.nbytes = self.nbytes_dict[self.dtype]
        self.minVal = self.limits_dict[self.dtype]['min']
        self.maxVal = self.limits_dict[self.dtype]['max']
        self.file_header = str.encode(
                                      '0000002048\r\n' +
                                      'FILE_DATA_BEGIN=0000002048\r\n' +
                                      'FILE_BYTE_ORDERING=LITTLE-ENDIAN\r\n' +
                                      'FILE_NUMERICAL_FORMAT='+str(self.dtype)+'\r\n' +
                                      'FRAME_HEADER_SIZE='+str(self.frhsize)+'\r\n' +
                                      'IMAGE_ADJUST_INCREMENT=1\r\n' +
                                      'IMAGE_ADJUST_RESOLUTION=0\r\n' +
                                      'IMAGE_HEIGHT='+str(self.imgH)+'\r\n' +
                                      'IMAGE_MAX_PIXEL_VALUE='+str(self.maxVal)+'\r\n' +
                                      'IMAGE_MIN_PIXEL_VALUE='+str(self.minVal)+'\r\n' +
                                      'IMAGE_WIDTH='+str(self.imgW)+'\r\n'
                                     )
        self.frame_header = None
        self.meta_header = None
        self.foffset = 0 # the file offset in bytes
        self.outfile = None # the file pointer
        self.img = None # the image from the current frame
        self._open_file() # open the file and write the file header

    def _open_file(self):
        self.outfile = open(self.filepath, 'wb')
        self.outfile.write(self.file_header)
        remaining = self.file_header_size - len(self.file_header)
        padding = str.encode(' '*remaining)
        self.outfile.write(padding)
        self.foffset += self.file_header_size
        
    def write_next_frame(self, img, frameHeader=None, metaHeader=None):

        # write this file header (if exists)
        if frameHeader:
            self.frame_header = str.encode(frameHeader)
            self.outfile.write(self.frame_header)
            remaining = self.frhsize - len(self.frame_header)
            padding = str.encode(' '*remaining)
            self.outfile.write(padding)
            self.foffset += self.frhsize

        # write this meta header (if exists)
        if metaHeader:
            self.meta_header = str.encode(metaHeader)
            self.outfile.write(self.meta_header)
            remaining = self.metasize - len(self.meta_header)
            padding = str.encode(' '*remaining)
            self.outfile.write(padding)
            self.foffset += self.metasize

        # write this image data
        frame = np.reshape(img, self.imgH*self.imgW*self.depth)
        binframe = struct.pack(self.fmt, *frame)
        self.outfile.write(binframe)
        self.foffset += struct.calcsize(self.fmt)

    def close_file(self):
        self.outfile.close()

