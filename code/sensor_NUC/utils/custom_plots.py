import matplotlib.pyplot as plt
import numpy as np
import warnings
from skimage.color import gray2rgb
from skimage import img_as_ubyte
import matplotlib.patches as patches


warnings.filterwarnings("ignore", category=UserWarning)
plt.style.use('dark_background')
# https://matplotlib.org/examples/color/named_colors.html
colors = {1:'deepskyblue', 2:'lightskyblue', 3:'limegreen', 4:'orangered',
          5:'red', 6:'gold', 7:'snow', 8:'black'}

fc='black'
cm = 'gray'
theme_col = colors[6]
fig, ax = plt.subplots(1, 1, figsize=(8.75,7))
#fig, ax = plt.subplots(1, 1, figsize=(12,4)) # double image # good aspect ratio
#fig, ax = plt.subplots(1, 1, figsize=(9,5)) # a normal single image
#fig, ax = plt.subplots(1, 1, figsize=(15,12)) # a large single image

fig.patch.set_facecolor(fc)
fig.suptitle(" ")
    
    
def show_img(img, name, cm=cm, ask=True):
    plt.ion()
    plt.imshow(img, cmap=cm)
    plt.show()
    ax.spines['bottom'].set_color(theme_col)
    ax.spines['top'].set_color(theme_col)
    ax.spines['right'].set_color(theme_col)
    ax.spines['left'].set_color(theme_col)
    ax.tick_params(axis='x', colors=theme_col)
    ax.tick_params(axis='y', colors=theme_col)
    ax.yaxis.label.set_color(theme_col)
    ax.xaxis.label.set_color(theme_col)
    if type(name)==int:
        ax.set_title('frame '+str(name), fontname='Hack')
    else:
        ax.set_title(name, fontname='Hack')
    ax.title.set_color(theme_col)
    
    plt.tight_layout()
    plt.draw()
    if ask:
        accept = input('')
    else:
        accept = 'y'
        plt.pause(0.0001)
    plt.cla()
    return(accept)

def show_img_with_bounding_boxes(imgRGB, rects_dict, name, cm=cm, ask=True):
    plt.ion()
    plt.imshow(imgRGB, cmap=cm)
    plt.show()
    ax.spines['bottom'].set_color(theme_col)
    ax.spines['top'].set_color(theme_col)
    ax.spines['right'].set_color(theme_col)
    ax.spines['left'].set_color(theme_col)
    ax.tick_params(axis='x', colors=theme_col)
    ax.tick_params(axis='y', colors=theme_col)
    ax.yaxis.label.set_color(theme_col)
    ax.xaxis.label.set_color(theme_col)
    if type(name)==int:
        ax.set_title('frame '+str(name), fontname='Hack')
    else:
        ax.set_title(name, fontname='Hack')
    ax.title.set_color(theme_col)

    for iobj in rects_dict:
        (y1,y2,x1,x2) = rects_dict[iobj]['yxs']
        y1 -= 1
        y2 += 1
        x1 -= 1
        x2 += 1
        rect_color = rects_dict[iobj]['color']
        plt.gca().add_patch(patches.Rectangle((x1,y1), x2-x1, y2-y1,
                                              edgecolor=rect_color,
                                              facecolor='none',
                                              lw=2))
    plt.tight_layout()
    plt.draw()
    if ask:
        accept = input('')
    else:
        accept = 'y'
        plt.pause(0.0001)
    plt.cla()
    return(accept)


def write_img(img, iframeStr, titleStr, out_dir, vmin, vmax, cm='gray'):
    plt.ion()
    plt.imshow(img, cmap=cm, vmin=vmin, vmax=vmax)
    #plt.imshow(img, cmap=cm)
    ax.spines['bottom'].set_color(theme_col)
    ax.spines['top'].set_color(theme_col)
    ax.spines['right'].set_color(theme_col)
    ax.spines['left'].set_color(theme_col)
    ax.tick_params(axis='x', colors=theme_col)
    ax.tick_params(axis='y', colors=theme_col)
    ax.yaxis.label.set_color(theme_col)
    ax.xaxis.label.set_color(theme_col)
    plt.xticks([])
    plt.yticks([])
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_title(titleStr+ " - " + iframeStr)
    ax.title.set_color(theme_col)  
    
    plt.savefig(out_dir+'/'+iframeStr, bbox_inches='tight')
    plt.cla()

def write_img_with_bounding_boxes(img, rects_dict, iframeStr, titleStr, out_dir, cm='gray'):
    plt.ion()
    plt.imshow(img, cmap=cm)
    #plt.imshow(img, cmap=cm)
    ax.spines['bottom'].set_color(theme_col)
    ax.spines['top'].set_color(theme_col)
    ax.spines['right'].set_color(theme_col)
    ax.spines['left'].set_color(theme_col)
    ax.tick_params(axis='x', colors=theme_col)
    ax.tick_params(axis='y', colors=theme_col)
    ax.yaxis.label.set_color(theme_col)
    ax.xaxis.label.set_color(theme_col)
    plt.xticks([])
    plt.yticks([])
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_title(titleStr+ " - " + iframeStr)
    ax.title.set_color(theme_col)

    for iobj in rects_dict:
        (y1,y2,x1,x2) = rects_dict[iobj]['yxs']
        y1 -= 1
        y2 += 1
        x1 -= 1
        x2 += 1
        rect_color = rects_dict[iobj]['color']
        plt.gca().add_patch(patches.Rectangle((x1,y1), x2-x1, y2-y1,
                                              edgecolor=rect_color,
                                              facecolor='none',
                                              lw=2))
 
    plt.savefig(out_dir+'/'+iframeStr, bbox_inches='tight')
    plt.cla()
