'''
Read raw sensor sequence, apply NUC and BPR to frames.
'''


import os
import argparse
import pickle
import h5py
import numpy as np
from skimage.draw import polygon
from skimage.registration import phase_cross_correlation
from skimage.transform import warp
from skimage.morphology import remove_small_objects, binary_dilation, square
import utils.BinaryFile as BinaryFile
from utils import neighbors
from utils import custom_plots
from utils.image_utils import *
from utils import clustering


''' PARSE ARGS --------------------------------------------------------------------------------'''
parser = argparse.ArgumentParser()
parser.add_argument("-d", help="display each frame", action="store_true")
parser.add_argument("-p", help="pause on each displed frame", action="store_true")
parser.add_argument("-w", help="write each frame to output dir", action="store_true")
args = parser.parse_args()
DISPLAY = False
PAUSE = False
WRITE = False
if args.d:
    DISPLAY = True
if args.p:
    PAUSE = True
if args.w:
    WRITE = True

''' SETTINGS ----------------------------------------------------------------------------------'''
raw_filepath = "../Data/infile.raw"
outDir = "./output"
nuc_file = 'sensor_NUC.h5'
bad_cands_file = 'sensor_bad_candidates.pkl'

startFrame = 300 #0
nextFrameSkip = 0 # skip this many between successive frames
nbRead = -1 # read this many frames

raw_file = BinaryFile.Reader(raw_filepath, startFrame, nextFrameSkip, nbRead)

total_frames = raw_file.totalFrames
imgH = raw_file.imgH
imgW = raw_file.imgW
print("total frames:", total_frames)

hf = h5py.File(nuc_file, 'r')
Gain_mw = hf['Gain_mw'].value
Offset_mw = hf['Offset_mw'].value
BadPix_mw = hf['BadPix_mw'].value
Gain_lw = hf['Gain_lw'].value
Offset_lw = hf['Offset_lw'].value
BadPix_lw = hf['BadPix_lw'].value
hf.close()

pklfile = open('sensor_bads_candidates.pkl', 'rb')      
bads_cands = pickle.load(pklfile)
pklfile.close() 
[ bads_mw, bads_lw, candidates_mw, candidates_lw ] = bads_cands

imgW = imgW // 2

''' Identify pixels belonging to the component polarization images '''
dr,dc = (0.25, 0.25)
r, c = (int(imgH/2), int(imgW/2))
print('r:', r, '  c:', c)


''' Define a mask '''
subH, subW = (imgH//2, imgW//2)
mask = np.zeros((subH,subW), np.uint8)
v1 = (0,0)
v2 = (0,subW-1)
v3 = (subH-1,subW-1)
v4 = (subH-1, 233)
v5 = (546, 0)
poly_vertices = np.array( (v1,v2,v3,v4,v5) )
rr,cc = polygon(poly_vertices[:,0], poly_vertices[:,1], mask.shape)
mask[rr,cc] = 1

''' Define a mask for the smaller polarization product images '''

submask = np.zeros((r, c), np.float32)
v1 = (0,0)
v2 = (0,c-1)
v3 = (r-1,c-1)
v4 = (r-1,130)
v5 = (260,0)
poly_vertices = np.array( (v1,v2,v3,v4,v5) )
rr,cc = polygon(poly_vertices[:,0], poly_vertices[:,1], submask.shape)
submask[rr,cc] = 1

# Iterate through the frames of the sequences
frame_count = 0
stack_size = 10
lookback = 9
max_pixel_size = 1e4
s0_stack = np.empty((stack_size,r,c), np.float64)
dolp_stack = np.empty((stack_size,r,c), np.float64)
while(raw_file.isOpen):
    imgnumb = str(raw_file.frame_number).zfill(4)
    timestamp = raw_file.frame_header
    print(timestamp, ' ', imgnumb)

    img = raw_file.get_next_image().squeeze()
    img_mw = img[:,0:imgW]
    img_lw = img[:,imgW:]
    #custom_plots.show_img(img_mw, 'img_mw '+imgnumb, ask=PAUSE)
    #custom_plots.show_img(img_lw, 'img_lw '+imgnumb, ask=PAUSE)

    ''' Apply NUC and BPR '''
    img_mw = (img_mw * Gain_mw) + Offset_mw
    img_mw[img_mw<0] = 0
    img_mw[img_mw>65535] = 65535
    img_mw = neighbors.replaceBadPixels(img_mw, bads_mw, candidates_mw, mask)

    img_lw = (img_lw * Gain_lw) + Offset_lw
    img_lw[img_lw<0] = 0
    img_lw[img_lw>65535] = 65535
    img_lw = neighbors.replaceBadPixels(img_lw, bads_lw, candidates_lw, mask)

    #custom_plots.show_img(img_mw, 'img_mw '+imgnumb, ask=PAUSE)
    #custom_plots.show_img(img_lw, 'img_lw '+imgnumb, ask=PAUSE)

    frame_count += 1
