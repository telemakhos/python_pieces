'''
Calculate the dual-band sensor NUC.
'''


import os
import argparse
import pickle
import h5py
import numpy as np
from skimage.draw import polygon
import utils.BinaryFile as BinaryFile
from utils import neighbors
from utils import custom_plots


''' PARSE ARGS --------------------------------------------------------------------------------'''
parser = argparse.ArgumentParser()
parser.add_argument("-d", help="display each frame", action="store_true")
parser.add_argument("-p", help="pause on each displed frame", action="store_true")
parser.add_argument("-w", help="write each frame to output dir", action="store_true")
args = parser.parse_args()
DISPLAY = False
PAUSE = False
WRITE = False
if args.d:
    DISPLAY = True
if args.p:
    PAUSE = True
if args.w:
    WRITE = True

''' SETTINGS ----------------------------------------------------------------------------------'''
data_dir = "../Data"
cal_dir = os.path.join(data_dir, "cal")
low_mw_filepath = os.path.join(cal_dir, "low_mw.raw")
low_lw_filepath = os.path.join(cal_dir, "low_lw.raw")
high_mw_filepath = os.path.join(cal_dir, "high_mw.raw")
high_lw_filepath = os.path.join(cal_dir, "high_lw.raw")

startFrame = 0
nextFrameSkip = 0 # skip this many between successive frames
nbRead = -1 # read this many frames

low_mw_file = BinaryFile.Reader(low_mw_filepath, startFrame, nextFrameSkip, nbRead)
low_lw_file = BinaryFile.Reader(low_lw_filepath, startFrame, nextFrameSkip, nbRead)
high_mw_file = BinaryFile.Reader(high_mw_filepath, startFrame, nextFrameSkip, nbRead)
high_lw_file = BinaryFile.Reader(high_lw_filepath, startFrame, nextFrameSkip, nbRead)

total_frames = low_mw_file.totalFrames
imgH = low_mw_file.imgH
imgW = low_mw_file.imgW
print("low_mw_file total frames:", total_frames)
total_frames = low_lw_file.totalFrames
print("low_lw_file total frames:", total_frames)
total_frames = high_mw_file.totalFrames
print("high_mw_file total frames:", total_frames)
total_frames = high_lw_file.totalFrames
print("high_lw_file total frames:", total_frames)

#''' Define a submask '''
#submask = np.zeros((imgH, imgW), np.uint8)
#v1 = (0,0)
#v2 = (0,imgW-1)
#v3 = (imgH-1,imgW-1)
#v4 = (imgH-1, 233)
#v5 = (546, 0)
#poly_vertices = np.array( (v1,v2,v3,v4,v5) )
#rr,cc = polygon(poly_vertices[:,0], poly_vertices[:,1], submask.shape)
#submask[rr,cc] = 1


Img_low_mws = []
Img_high_mws = []
Img_low_lws = []
Img_high_lws = []
# Iterate through the frames of the sequences and save to lists
while(low_mw_file.isOpen):
    low_mw = low_mw_file.get_next_image().squeeze()
    low_lw = low_lw_file.get_next_image().squeeze()
    high_mw = high_mw_file.get_next_image().squeeze()
    high_lw = high_lw_file.get_next_image().squeeze()

    Img_low_mws.append(low_mw)
    Img_low_lws.append(low_lw)
    Img_high_mws.append(high_mw)
    Img_high_lws.append(high_lw)

# lists to NumPy arrays
mw_low_arr = np.array(Img_low_mws)
lw_low_arr = np.array(Img_low_lws)
mw_high_arr = np.array(Img_high_mws)
lw_high_arr = np.array(Img_high_lws)



'''1. Compute low temperature scene average for both wavebands, for both temperatures'''
mw_low_mean = np.mean(mw_low_arr, axis=0)
mw_high_mean = np.mean(mw_high_arr, axis=0)
lw_low_mean = np.mean(lw_low_arr, axis=0)
lw_high_mean = np.mean(lw_high_arr, axis=0)

'''2. Compute temporal standard deviation image from the low temperature sequences,
      for both wavebands'''
#MWIR
std_temporal_mw_low  = np.std(mw_low_arr, axis=0)
std_mean = np.mean(std_temporal_mw_low)
std_std  = np.std(std_temporal_mw_low)
std_temporal_mw_low = np.abs(std_temporal_mw_low - std_mean) / std_std

# LWIR
std_temporal_lw_low  = np.std(lw_low_arr, axis=0)
std_mean = np.mean(std_temporal_lw_low)
std_std  = np.std(std_temporal_lw_low)
std_temporal_lw_low = np.abs(std_temporal_lw_low - std_mean) / std_std


'''3. Compute spatial standard deviation frame for both wavebands for both temperatures'''
# MWIR
mean_img = mw_low_mean
mean_val = np.mean(mean_img)
std_val  = np.std(mean_img)
std_spatial_mw = np.abs(mean_img - mean_val) / std_val

# LWIR
mean_img = lw_low_mean
mean_val = np.mean(mean_img)
std_val  = np.std(mean_img)
std_spatial_lw = np.abs(mean_img - mean_val) / std_val

'''4. Compute gain and offset for both wavebands'''
# MWIR
waveband_range_mw = np.mean(mw_high_mean) - np.mean(mw_low_mean)
pixel_range_mw = mw_high_mean - mw_low_mean
pixel_range_mw[pixel_range_mw==0] = 1
Gain_mw = waveband_range_mw / pixel_range_mw
Offset_mw = np.mean(mw_low_mean) - (Gain_mw * mw_low_mean)

# LWIR
waveband_range_lw = np.mean(lw_high_mean) - np.mean(lw_low_mean)
pixel_range_lw = lw_high_mean - lw_low_mean
pixel_range_lw[pixel_range_lw==0] = 1
Gain_lw = waveband_range_lw / pixel_range_lw
Offset_lw = np.mean(lw_low_mean) - (Gain_lw * lw_low_mean)

'''5. Compute bad pixel maps'''
# MWIR
BadPix_mw = np.zeros((r, c), np.uint8)
BadPix_mw[ std_temporal_mw_low > 30 ] = 1 # flashers
BadPix_mw[ std_temporal_mw_low == 0 ] = 1 # stuck-ons
BadPix_mw[ std_spatial_mw > 4 ] = 1   # spatial anomalies
others = (np.array([272,288,288]), np.array([478,592,593]))
BadPix_mw[others] = 1                     # manually selected
nb_badPix_mw = len(np.where(BadPix_mw == 1)[0])
pcMWbad = (nb_badPix_mw / (r * c)) * 100
print( f'\nNumber of bad pixels from mw imagery: {nb_badPix_mw}, % of total pix: {pcMWbad:.2f}' )

# LWIR
BadPix_lw = np.zeros((r, c), np.uint8)
BadPix_lw[ std_temporal_lw_low > 30 ] = 1 # flashers
BadPix_lw[ std_temporal_lw_low == 0 ] = 1 # stuck-ons
BadPix_lw[ std_spatial_lw > 4 ] = 1   # spatial anomalies
others = (np.array([64]), np.array([252]))
BadPix_lw[others] = 1                     # manually selected
nb_badPix_lw = len(np.where(BadPix_lw == 1)[0])
pcLWbad = (nb_badPix_lw / (r * c)) * 100
print( f'Number of bad pixels from lw imagery: {nb_badPix_lw}, % of total pix: {pcLWbad:.2f}' )

if True:
    '''Save Gains, Offsets, and bad pixels to file'''
    out_NUC_file = 'sensor_NUC.h5'
    with h5py.File(out_NUC_file, 'w') as hf:
        print("\nWRITING FILE: ", "NUC")
    
        hf.create_dataset('Gain_mw', data=Gain_mw_135)
        hf.create_dataset('Offset_mw', data=Offset_mw_135)
        hf.create_dataset('BadPix_mw', data=BadPix_mw_135)
    
        hf.create_dataset('Gain_lw', data=Gain_lw_135)
        hf.create_dataset('Offset_lw', data=Offset_lw_135)
        hf.create_dataset('BadPix_lw', data=BadPix_lw_135)
    
    
    print("\nGain_mw: ", np.min(Gain_mw), np.mean(Gain_mw), np.max(Gain_mw))
    print("Gain_lw: ", np.min(Gain_lw), np.mean(Gain_lw), np.max(Gain_lw))
    
    print("\nOffset_mw: ", np.min(Offset_mw), np.mean(Offset_mw), np.max(Offset_mw))
    print("Offset_lw: ", np.min(Offset_lw), np.mean(Offset_lw), np.max(Offset_lw))
    
    '''6. Find replacements for each waveband (MWIR, LWIR)'''
    bads_mw, candidates_mw = neighbors.getReplacementIndexes(BadPix_mw, 2)
    bads_lw, candidates_lw = neighbors.getReplacementIndexes(BadPix_lw, 2)
    
    '''Save pixel replacement information to file'''
    out_bads_cands_file = 'sensor_bads_candidates.pkl'
    print("\nWRITING FILE: ", out_bads_cands_file)
    with open(out_bads_cands_file, 'wb') as pf:
        bads_cands = [bads_mw, bads_lw, candidates_mw, candidates_lw, candidates_lw_45]
        pickle.dump(bads_cands, pf, pickle.HIGHEST_PROTOCOL)      
        pf.close()
