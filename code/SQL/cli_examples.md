## **SQLite3 command line interface examples**  

---

**Install the command line interface for SQLite3 (Ubuntu):**  
`~$ sudo apt-get install sqlite3`  

**Start the interface:**  
`~$ sqlite3 starforce_personnel.db`  
SQLite version 3.31.1 2020-01-27 19:55:54  
Enter ".help" for usage hints.  

**Look at the pilots table:**  
`sqlite> select * from pilots;`     
1001|Xerxoid|0.978|700  
992|Memoisma|0.777|711  
223|Uhrlohna|0.885|723  
536|Schwifty|0.996|776  
109|Xuma|0.899|  

**View a single column:**  
`sqlite> select score from pilots;`    
0.978  
0.777  
0.885  
0.996  
0.899  

**List the table names:**  
`sqlite> .tables`    
engineers  pilots  

**A more advanced query:**  
`sqlite> select forceId, name from pilots where pilots.score > 0.85;`    
1001|Xerxoid  
223|Uhrlohna  
536|Schwifty   
109|Xuma  

**List the column names and datatype information:**  
`sqlite> PRAGMA table_info(engineers);`    
0|forceId|integer|0||0  
1|name|text|0||0  
2|score|real|0||0  
3|shipmateId|integer|0||0  
