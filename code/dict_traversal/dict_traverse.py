def dict_traverse(tree, key):
    if key in tree:
        return tree[key]
    for v in filter(dict.__instancecheck__, tree.values()):
        if (found := dict_traverse(v, key)) is not None:  
            return found
