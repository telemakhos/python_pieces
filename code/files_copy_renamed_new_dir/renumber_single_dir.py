'''

'''
import os
from shutil import copyfile


# returns a file path generator for all files under data_dir
def get_tracks(data_dir):
    fileiter = (os.path.join(root, f)
                for root, _, files in os.walk(data_dir)
                for f in files)
    return fileiter


original_dir = 'images_orig'
renumbered_dir = 'images'
file_extension = '.png'

# read all files in dir
trkiter = get_tracks(original_dir)
# sort the files
filez = sorted([x for x in trkiter])
count = 0
# rename the files in ascending order
for fz in filez:
    ixnum = str(count).zfill(4)
    newtrk = ixnum + file_extension
    print(fz)
    print(newtrk)
    print('')
    copyfile(fz, renumbered_dir + '/' + newtrk)
    count += 1
